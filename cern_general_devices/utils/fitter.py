import numpy as np
import matplotlib.pyplot as plt

from scipy import optimize


# funcs
def gaus(x, a1, mu1, sigma1, a0):
    return abs(a1) * np.exp(-(1 / 2.0) * ((x - mu1) / sigma1) ** 2) + a0


def get_moments(X, Y):
    mean = np.average(X, weights=np.abs(Y))
    sigma = np.sqrt(np.average(X ** 2, weights=np.abs(Y)) - mean ** 2)
    return mean, sigma


def gaussian_profile_fit(X, Y):
    mean = np.average(X, weights=np.abs(Y))
    sigma = np.sqrt(np.average(X ** 2, weights=np.abs(Y)) - mean ** 2)
    mask = (X <= 4 * sigma) & (X >= -4 * sigma)
    x_, y_ = X[mask], Y[mask]
    p0 = [y_.max() - y_.min(), mean, sigma, y_.min()]

    try:
        fit_val, fit_err = optimize.curve_fit(gaus, x_, y_, p0=p0)
        fit_success = True
    except:
        fit_val, fit_err = np.zeros((2, 4))
        fit_success = False

    else:
        fit_err = np.sqrt(np.diagonal(fit_err))

    fit_val[2] = np.abs(fit_val[2])
    return fit_val, fit_err, fit_success


# returns the level at a sigma value of the double gaussian fit
level = (
    lambda sigma, amplitude, offset: np.exp(-(sigma ** 2) / 2) * amplitude
    + offset
)


def plot_fit(fit, fit_err, ax, txt_fit="", alpha=1.0):
    Xmin, Xmax = ax.get_xlim()
    ax.plot(
        np.linspace(Xmin, Xmax, 1500),
        gaus(np.linspace(Xmin, Xmax, 1500), *fit),
        "r-",
    )

    # txt_fit += '$\chi^2/ndf={{{:5.3g}}}$ \n'.format(chis_ndf)
    txt_fit += "$\mu=\, {{{:5.3g}}}\pm{{{:5.3g}}}$ mm \n".format(
        fit[1], fit_err[1]
    )
    txt_fit += "$\sigma=\, {{{:5.3g}}}\pm{{{:5.3g}}}$ mm".format(
        fit[2], fit_err[2]
    )

    text = ax.text(
        0.1,
        0.85,
        txt_fit,
        ha="left",
        va="center",
        transform=ax.transAxes,
        bbox={"facecolor": "white", "alpha": 1.0, "pad": 5},
    )

    return


def twoD_Gaussian(
    xy_grid, amplitude, x0, y0, sigma_x, sigma_y, offset, theta=0
):
    x, y = xy_grid
    a = (np.cos(theta) ** 2) / (2 * sigma_x ** 2) + (np.sin(theta) ** 2) / (
        2 * sigma_y ** 2
    )
    b = -(np.sin(2 * theta)) / (4 * sigma_x ** 2) + (np.sin(2 * theta)) / (
        4 * sigma_y ** 2
    )
    c = (np.sin(theta) ** 2) / (2 * sigma_x ** 2) + (np.cos(theta) ** 2) / (
        2 * sigma_y ** 2
    )
    g = offset + amplitude * np.exp(
        -(
            a * ((x - x0) ** 2)
            + 2 * b * (x - x0) * (y - y0)
            + c * ((y - y0) ** 2)
        )
    )
    return g.ravel()


def BTV_fit(im, x_grid, y_grid, initial_guess=None, mask=None):

    try:
        popt, pcov = optimize.curve_fit(
            twoD_Gaussian,
            (
                np.ma.masked_array(x_grid, mask).compressed(),
                np.ma.masked_array(y_grid, mask).compressed(),
            ),
            np.ma.masked_array(im, mask).compressed(),
            p0=initial_guess,
        )
        fit_success = True
    # imarray_1d = im.ravel()
    # popt, pcov = optimize.curve_fit(twoD_Gaussian, (x_grid, y_grid),
    #                                 imarray_1d, p0=initial_guess)
    except:
        popt = np.zeros(6)
        popt_err = np.zeros(6)
        print("fit failure")
        fit_success = False
    else:
        try:
            popt_err = np.sqrt(np.diagonal(pcov))
        except:
            popt = np.zeros(6)
            popt_err = np.zeros(6)
            print("fit failure")
            fit_success = False
    popt[0] = abs(popt[0])
    popt[3] = abs(popt[3])
    popt[4] = abs(popt[4])
    return popt, popt_err, fit_success


def BTV_multi_fit(
    BTV_image, sigma_cut=4, number_iterations=5, get_all_fits=False
):

    fit, fit_err = BTV_fit(BTV_image)
    for i in range(number_iterations - 1):

        data_fitted = twoD_Gaussian((x_grid, y_grid), *last_fit).reshape(
            im.shape
        )
        data_fitted_cliped = np.ma.masked_less(
            data_fitted, level(sigma_cut, *last_fit[[0, 5]])
        )

        try:
            popt, perr = optimize.curve_fit(
                twoD_Gaussian,
                (
                    np.ma.masked_array(
                        x_grid, data_fitted_cliped.mask
                    ).compressed(),
                    np.ma.masked_array(
                        y_grid, data_fitted_cliped.mask
                    ).compressed(),
                ),
                np.ma.masked_array(im, data_fitted_cliped.mask).compressed(),
                p0=last_fit,
            )
        except (RuntimeError, OptimizeWarning) as e:
            print("failed optimization on iteration {:}".format(i))
            return (
                np.vstack((all_fits, np.zeros((number_iterations - i, 6)))),
                np.vstack(
                    (all_fits_err, np.zeros((number_iterations - i, 6)))
                ),
                i,
                all_fits[-1],
                all_fits_err[-1],
            )
        perr = np.sqrt(np.diag(perr))

        all_fits.append(popt)
        all_fits_err.append(perr)

    if get_all_fits:
        return


def BTV_process(BTV_image):
    x = BTV_image["imagePositionSet1"]
    y = BTV_image["imagePositionSet2"][::-1]
    x_grid, y_grid = np.meshgrid(x, y)

    dx = BTV_image["pixelCalSet1"][0]
    dy = BTV_image["pixelCalSet2"][0]
    extent = [x[0] - dx, x[-1] + dx, y[0] - dy, y[-1] + dy]

    return x, y, x_grid, y_grid, dx, dy, extent


def gaussian_estimate(x, y):
    # estimates the gaussian parameters from positions x and bin heights y
    # y = h + c * exp(-(x-a)**2/b)

    S = np.zeros(len(x))
    T = np.zeros(len(x))

    for k in range(1, len(x)):
        S[k] = S[k - 1] + 1 / 2 * (y[k] + y[k - 1]) * (x[k] - x[k - 1])
        T[k] = T[k - 1] + 1 / 2 * (x[k] * y[k] + x[k - 1] * y[k - 1]) * (
            x[k] - x[k - 1]
        )

    M = np.matrix(
        [
            [
                (S ** 2).sum(),
                (S * T).sum(),
                (S * (x ** 2 - x[0] ** 2)).sum(),
                (S * (x - x[0])).sum(),
            ],
            [
                (S * T).sum(),
                (T ** 2).sum(),
                (T * (x ** 2 - x[0] ** 2)).sum(),
                (T * (x - x[0])).sum(),
            ],
            [
                (S * (x ** 2 - x[0] ** 2)).sum(),
                (T * (x ** 2 - x[0] ** 2)).sum(),
                ((x ** 2 - x[0] ** 2) ** 2).sum(),
                ((x ** 2 - x[0] ** 2) * (x - x[0])).sum(),
            ],
            [
                (S * (x - x[0])).sum(),
                (T * (x - x[0])).sum(),
                ((x ** 2 - x[0] ** 2) * (x - x[0])).sum(),
                ((x - x[0]) ** 2).sum(),
            ],
        ]
    )

    V = np.matrix(
        [
            [(S * (y - y[0])).sum()],
            [(T * (y - y[0])).sum()],
            [((x ** 2 - x[0] ** 2) * (y - y[0])).sum()],
            [((x - x[0]) * (y - y[0])).sum()],
        ]
    )

    ABCD = M.I * V
    A, B, C, D = np.array(ABCD).squeeze()

    a = -A / B
    b = -2 / B

    t = np.exp(-((x - a) ** 2) / b)

    m1 = np.matrix([[(t ** 2).sum(), t.sum()], [t.sum(), len(x)]])
    m2 = np.matrix([[(t * y).sum()], [y.sum()]])

    c, h = np.array(m1.I * m2).squeeze()

    return h, c, a, np.sqrt(b / 2)
