class SPS:
    def __init__(self) -> None:

        self.start_sc_var = "XTIM.SX.SSC-CT"

        self.start_cycle = "XTS.INJF1KFO-XTIM"
        self.injection = "XTS.INJAMC-XTIM"

        self.start_ramp = "XTS.S-RAMP-XTIM"

        self.start_ft = "XTIM.SX.SFLATTOP-CT"
        self.start_se = "SEX.S-AMC-CTML"

        self.beam_out = "XTIM.SX.BEAMOUT-CT"
        self.bis_open = "SPS.BST.BIS-OPEN"

        self.acq = "Acquisition"

        for key, value in self.__dict__.items():
            if key is not "acq":
                self.__setattr__(key, "/".join([value, self.acq]))

        self.intensity_performance = "SPSQC/INTENSITY.PERFORMANCE"
        self.extrcted_intensity = "extractedIntensity"