from cern_general_devices.afg import AFG
import numpy as np

instrument = AFG("128.141.184.20", "rfko afg")

F_rev = 470000 # Hz

instrument.configureRFKO(
    center_frequency = F_rev/3,         # 1/3rd integer resonance
    frequency_deviation = F_rev/10,     # range around 1/10th
    repeition_rate = 100,               # 10ms interval
    gain = 1                            # Qmeter Gain 1 = 2 Vpp
)

instrument.getParameter("SOUR1:FREQ:CW?")
instrument.setParameter("SOUR1:VOLT", "500", "mVpp")

for rep_rate in np.linspace(100, 500, 5):
    instrument.setRepRate(rep_rate)
