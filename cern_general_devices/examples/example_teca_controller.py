from cern_general_devices.tecs import TECA
import pyjapc
import matplotlib.pyplot as plt
import numpy as np

japc = pyjapc.PyJapc("SPS.USER.SFTPRO3")

teca = TECA(japc=japc, timingSelectorOverride="", start_monitoring=True)

target_settings = [34.1, -1680.0]

pid_gains = dict(
    ki=np.array([0.0, 0.0]),
    kp=np.array([0.6, 0.6]),
    kd=np.array([0.0, 0.0]),
)
teca.set_lvdt_values(target_settings, pid_gains)
