import pyjapc
import matplotlib.pyplot as plt
from cern_general_devices.bpm import BPMALPS
from cern_general_devices.bct import BCTDC
from cern_general_devices.cpu_mkp import CPUMKP
from cern_general_devices.temperature_sps import MKTemperature
import numpy as np
from dataclasses import dataclass, field
from collections import deque

japc = pyjapc.PyJapc("SPS.USER.MD5")

bpms = BPMALPS(japc, 2)
bct = BCTDC(japc)
cpu_mkp = CPUMKP(japc)
mks_temp = MKTemperature(japc)


@dataclass
class Buffers:
    buffer_length: int = 10
    delays: deque = field(default_factory=deque)

    def __post_init__(self):
        self.delays = deque(maxlen=self.buffer_length)

    def np_delays(self):
        return np.array(self.delays)


data, info = bpms.getParameter("trajectory1")
data_orbit, info = bpms.getParameter("orbit")

data["hor"]
data_orbit["hor"].shape

data["hor"] - data["hor"][1, :]

bpms_names = bpms.channel_names[bpms.mask_h]

plt.figure()
plt.bar(bpms_names, data["hor"][1, :])
plt.xticks(rotation=90)
plt.show()

plt.figure()
plt.title(bpms_names[0])
plt.plot(data["hor"][:, 0])
plt.show()

cpu_list = []
cpu_list2 = []


def callback_cpu(param, value, header):
    global cpu_list
    if not header[0]["isFirstUpdate"]:
        cpu_list.append(value[0])
        cpu_list2.append(value[1])
        print(header[0]["cycleStamp"])


delays_time, action_time, temp_time = [], [], []


def callback(param, value, header):
    global delays_time, action_time
    if not header[0]["isFirstUpdate"]:
        cycle_stamps = [ele["cycleStamp"] for ele in header]
        try:
            dumped_intensity = bct.calculate_dumped_intensity(value[0]) / 1e11
        except TypeError:
            dumped_intensity = 0.0
        print(dumped_intensity)
        if (
            all(
                [
                    cycle_stamp == cycle_stamps[0]
                    for cycle_stamp in cycle_stamps
                ]
            )
            and dumped_intensity > 1e-3
        ):
            print(cycle_stamps[0])
            delays, _ = cpu_mkp.get_delays()
            temp_mkp_all, _ = mks_temp.getParameter(ele_name="mkp")
            temp_mkp4 = np.array(temp_mkp_all[-2:])
            print(delays)
            data_x, data_y = None, None
            for i in range(1, 5):
                try:
                    data_x, data_y = bpms.analyse_trajectory_data(
                        value[1], injection_event=i
                    )
                    print(f"Injection {i}")
                    action = np.max(data_x[1, :])
                except:
                    print("No valid data")
                    break
            if data_x is not None:
                action_time.append(action)
                delays_time.append(delays)
                temp_time.append(temp_mkp4)
                print(action_time)
                print(temp_time)


cpu1 = "MKP.BA1.IPOC.CPU-UP.7A/Waveform#waveformData"
cpu2 = "MKP.BA1.IPOC.CPU-DW.7A/Waveform#waveformData"
japc.subscribeParam(
    parameterName=[
        bct.bct5_dumped_intensity_var,
        bpms.trajectory_parameterName,
    ],
    onValueReceived=callback,
    getHeader=True,
)
# japc.subscribeParam(
#     parameterName=[cpu1, cpu2],
#     onValueReceived=callback_cpu,
#     getHeader=True,
# )

japc.startSubscriptions()
japc.stopSubscriptions()

japc.clearSubscriptions()
