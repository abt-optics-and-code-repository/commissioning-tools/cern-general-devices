import tkinter as tk
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg,
    NavigationToolbar2Tk,
)
import matplotlib.pyplot as plt
from cern_general_devices.tecs import TECA
import pyjapc


class LivePlotApp:
    def __init__(self, root: tk.Tk) -> None:
        self.root = root
        self.root.title("Live Plot App")

        # Initialize TECA and PyJapc
        self.japc = pyjapc.PyJapc("", incaAcceleratorName=None)
        self.teca = TECA(japc=self.japc, start_monitoring=True)

        # Create figure and axis
        self.fig, self.axis = plt.subplots(nrows=1, ncols=2, figsize=(8, 3))

        # Embed the figure in the tkinter window
        self.canvas = FigureCanvasTkAgg(self.fig, master=root)
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        # Add the matplotlib toolbar
        self.toolbar = NavigationToolbar2Tk(self.canvas, root)
        self.toolbar.update()
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        # Start updating the plots
        self.update_plots()

    def update_plots(self) -> None:
        [ax.cla() for ax in self.axis]
        try:
            self.teca._get_live_data()
            self.axis[0].plot(
                self.teca.teca_hystory["time"],
                self.teca.teca_hystory["measured_position_ctrl"],
                "--o",
                label="measured_position_ctrl",
            )
            self.axis[0].plot(
                self.teca.teca_hystory["time"],
                self.teca.teca_hystory["measured_position"],
                "--o",
                label="measured_position",
            )
            self.axis[1].plot(
                self.teca.teca_hystory["time"],
                self.teca.teca_hystory["measured_angle_ctrl"],
                "--o",
                label="measured_angle_ctrl",
            )
            self.axis[1].plot(
                self.teca.teca_hystory["time"],
                self.teca.teca_hystory["measured_angle"],
                "--o",
                label="measured_angle",
            )
            self.axis[1].legend(loc=3)
            self.axis[0].legend(loc=3)
            self.axis[0].set_ylabel("Position / mm")
            self.axis[1].set_ylabel(r"Angle / $\mu$rad")
            self.fig.canvas.draw_idle()
            self.fig.canvas.flush_events()
        except KeyError as e:
            print("No history yet")
            print(e)

        # Schedule the next update
        self.root.after(1000, self.update_plots)


if __name__ == "__main__":
    root = tk.Tk()
    app = LivePlotApp(root)
    root.mainloop()
