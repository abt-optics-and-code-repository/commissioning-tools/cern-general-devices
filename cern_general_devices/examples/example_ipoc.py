import numpy as np
import matplotlib.pyplot as plt
import pyjapc
from cern_general_devices import ipoc

japc = pyjapc.PyJapc()
japc.setSelector("SPS.USER.ALL")
ipoc_ins = ipoc.IPOC(japc)

data, time = ipoc_ins.get_mkdv_waveform(timingSelectorOverride="SPS.USER.SFTPRO1")

plt.figure()
plt.plot(time, data[2])
plt.show()
