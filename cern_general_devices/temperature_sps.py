import numpy as np
from cern_general_devices import Device
import logging


class MKTemperature(Device):
    def __init__(self, japc, timingSelectorOverride=None):
        """
        Class that handles the monitoring of the SPS kicker temperature
        Args:
            japc (PyJapc): Instance of PyJapc object
            timingSelectorOverride (str, optional): Timing selector override,
            for example: SPS.USER.SFTPRO2. Defaults to None.
        """

        super().__init__(japc, "bct", timingSelectorOverride)

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.data = []
        self.kickers = ["mkp", "mkdh", "mkdv", "mke4", "mke6"]

        self.temp_mkp_var = [
            "MKP.BA1.GEN/Temperature#temp1MKP1",
            "MKP.BA1.GEN/Temperature#temp2MKP1",
            "MKP.BA1.GEN/Temperature#temp1MKP4",
            "MKP.BA1.GEN/Temperature#temp2MKP4",
        ]

        self.temp_mkdh_var = [
            "SBDS.ECA5.SBDS5/Temperature#tempMkdH1",
            "SBDS.ECA5.SBDS5/Temperature#tempMkdH2",
            "SBDS.ECA5.SBDS5/Temperature#tempMkdH3",
        ]

        self.temp_mkdv_var = [
            "SBDS.ECA5.SBDS5/Temperature#tempMkdV1",
            "SBDS.ECA5.SBDS5/Temperature#tempMkdV2",
            "SBDS.ECA5.SBDS5/Temperature#tempMkdV3",
        ]

        self.temp_mke4_var = [
            "MKE.HCA421.GEN/Temperature#tempDownMke1",
            "MKE.HCA421.GEN/Temperature#tempDownMke2",
            "MKE.HCA421.GEN/Temperature#tempDownMke3",
            "MKE.HCA421.GEN/Temperature#tempDownMke4",
            # "MKE.HCA421.GEN/Temperature#tempDownMke5",
        ]

        self.temp_mke6_var = [
            "MKE.BA6.GEN/Temperature#tempDownMke1",
            "MKE.BA6.GEN/Temperature#tempDownMke2",
            "MKE.BA6.GEN/Temperature#tempDownMke3",
            # "MKE.BA6.GEN/Temperature#tempDownMke4",
        ]

        self.all_vars = (
            self.temp_mkp_var
            + self.temp_mkdh_var
            + self.temp_mkdv_var
            + self.temp_mke4_var
            + self.temp_mke6_var
        )

    def getParameter(self, ele_name=None, getHeader=True, **kwargs):
        info = {}

        ele_name = ele_name if ele_name in self.kickers else None

        if ele_name == "mkp":
            vars = self.temp_mkp_var
        elif ele_name == "mkdh":
            vars = self.temp_mkdh_var
        elif ele_name == "mkdv":
            vars = self.temp_mkdv_var
        elif ele_name == "mke4":
            vars = self.temp_mke4_var
        elif ele_name == "mke6":
            vars = self.temp_mke6_var
        else:
            vars = self.all_vars

        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        data = (
            self.japc.getParam(vars, getHeader, **kwargs)
            if getHeader
            else (
                self.japc.getParam(vars, getHeader, **kwargs),
                info,
            )
        )
        return data
