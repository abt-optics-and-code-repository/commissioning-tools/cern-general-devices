import numpy as np
from cern_general_devices import Device
import matplotlib.pyplot as plt
import logging
from typing import Tuple, Dict, List
import pandas as pd


class BPMOPS(Device):
    def __init__(self, japc, name, timingSelectorOverride=None):
        """Class to handle BPMOPS FESA class of PS BPM devices

        Use the expert GUI BPM app (TMS) to set-up the acquisition settings

        Args:
            japc (PyJapc): Instance of PyJapc object
            name (str): device name
            timingSelectorOverride (str, optional): Timing selector override, for example: CPS.USER.TOF. Defaults to None.
        """
        # Dictionaries to store acquired data
        self.acq = {}
        self.acq_info = {}

        super().__init__(japc, name, timingSelectorOverride)

    def getParameter(
        self, property="AcquisitionOrbit", getHeader=True, **kwds
    ):

        data_raw = self.japc.getParam(
            self.name + "/" + property,
            getHeader=getHeader,
            timingSelectorOverride=self.cycle_selector_override,
            **kwds,
        )

        self.acq[property], self.acq_info[property] = (
            data_raw if getHeader else (data_raw, {})
        )

        return self.acq[property], self.acq_info[property]

    def getOrbit(self, property="AcquisitionOrbit", getHeader=True, **kwds):

        data_raw = self.japc.getParam(
            self.name + "/" + property,
            getHeader=getHeader,
            timingSelectorOverride=self.cycle_selector_override,
            **kwds,
        )

        self.acq[property], self.acq_info[property] = (
            data_raw if getHeader else (data_raw, {})
        )

        # Separate H and V BPM's
        self.acq[property]["channelNamesH"] = self.acq[property][
            "channelNames"
        ][:43]
        self.acq[property]["channelNamesV"] = self.acq[property][
            "channelNames"
        ][43:]
        self.acq[property]["positionH"] = (
            self.acq[property]["position"][:43].astype(np.float) / 100.0
        )
        self.acq[property]["positionV"] = (
            self.acq[property]["position"][43:].astype(np.float) / 100.0
        )

        return self.acq[property], self.acq_info[property]

    def getRMS(self, channelName, position, plane="H", excludeBPM=None):
        if excludeBPM:
            delete_idx = []
            for i in excludeBPM:
                if i in channelName:
                    delete_idx.append(np.where(channelName == i)[0][0])

            position = np.delete(position, delete_idx, axis=0)
            print(
                "Removed BPMs "
                + str(channelName[delete_idx])
                + " from "
                + plane
                + " RMS computation"
            )
            print("Check length of array " + str(position.shape))
        return np.std(position, axis=0)

    def getTrajectoryBBB(
        self, property="AcquisitionTrajectoryBBB", getHeader=True, **kwds
    ):

        data_raw = self.japc.getParam(
            self.name + "/" + property,
            getHeader=getHeader,
            timingSelectorOverride=self.cycle_selector_override,
            **kwds,
        )

        self.acq[property], self.acq_info[property] = (
            data_raw if getHeader else (data_raw, {})
        )

        # Separate H and V BPM's
        self.acq[property]["channelNamesH"] = self.acq[property][
            "channelNames"
        ][:43]
        self.acq[property]["channelNamesV"] = self.acq[property][
            "channelNames"
        ][43:]
        self.acq[property]["positionH"] = (
            self.acq[property]["position"][:43].astype(np.float) / 100.0
        )  # [mm]
        self.acq[property]["positionV"] = (
            self.acq[property]["position"][43:].astype(np.float) / 100.0
        )  # [mm]

        # Find populated bunch index (only grabs a single bunch with maximum intensity):
        self.acq[property]["bunch_idx"] = (
            np.argmax(self.acq[property]["meanSigma"]) + 1
        )

        # Find populated bucket index array:
        self.acq[property]["bucket_idx"] = np.where(
            self.acq[property]["bucketId"] == self.acq[property]["bunch_idx"]
        )[0]

        return self.acq[property], self.acq_info[property]

    def plotOrbit(self, acq_idx):
        figName = (
            "BPM orbit for "
            + self.name
            + " at acquisition index "
            + str(acq_idx)
        )

        if not plt.fignum_exists(figName):
            plt.subplots(ncols=1, nrows=2, figsize=(10, 6), num=figName)

        ax = plt.figure(figName).axes

        ax[0].clear()
        ax[1].clear()

        ax[0].plot(
            [
                i.strip("PR.").strip(".H")
                for i in self.acq["AcquisitionOrbit"]["channelNamesH"]
            ],
            self.acq["AcquisitionOrbit"]["positionH"][:, acq_idx],
        )

        ax[0].tick_params(axis="x", rotation=90)
        ax[0].set_xlabel("BPM number")
        ax[0].set_ylabel(r"H [mm]")
        ax[0].set_title(self.acq_info["AcquisitionOrbit"]["acqStamp"])

        ax[1].plot(
            [
                i.strip("PR.").strip(".V")
                for i in self.acq["AcquisitionOrbit"]["channelNamesV"]
            ],
            self.acq["AcquisitionOrbit"]["positionV"][:, acq_idx],
        )

        ax[1].tick_params(axis="x", rotation=90)
        ax[1].set_xlabel("BPM number")
        ax[1].set_ylabel(r"V [mm]")

        plt.figure(figName).tight_layout()
        plt.pause(0.01)

    def plotOrbitEvolution(self, bpm_name=None, RMS=False, excludeBPM=[]):
        if bpm_name:
            try:
                if bpm_name[-1] == "H":
                    bpm_index = np.where(
                        self.acq["AcquisitionOrbit"]["channelNamesH"]
                        == bpm_name
                    )[0][0]
                elif bpm_name[-1] == "V":
                    bpm_index = np.where(
                        self.acq["AcquisitionOrbit"]["channelNamesV"]
                        == bpm_name
                    )[0][0]

                figName = (
                    "Orbit evolution for " + self.name + " at " + bpm_name
                )
                if not plt.fignum_exists(figName):
                    plt.subplots(
                        ncols=1, nrows=1, figsize=(10, 3), num=figName
                    )

                ax = plt.figure(figName).axes

                ax[0].clear()
                ax[0].plot(
                    self.acq["AcquisitionOrbit"]["measStamp"],
                    self.acq["AcquisitionOrbit"]["position" + bpm_name[-1]][
                        bpm_index, :
                    ],
                )

                ax[0].set_xlabel("Acquisition [ms]")
                ax[0].set_ylabel(bpm_name[-1] + " [mm]")
                ax[0].set_title(
                    bpm_name
                    + " "
                    + str(self.acq_info["AcquisitionOrbit"]["acqStamp"])
                )
                plt.figure(figName).tight_layout()
                plt.pause(0.01)

            except:
                logging.warning("BPM name incorrect or not found!")
        else:
            figName = "Orbit evolution " + self.name
            if not plt.fignum_exists(figName):
                plt.subplots(ncols=1, nrows=2, figsize=(8, 6), num=figName)

            ax = plt.figure(figName).axes

            ax[0].clear()
            ax[1].clear()

            dataH = self.acq["AcquisitionOrbit"]["positionH"][:, :].T

            ax[0].plot(self.acq["AcquisitionOrbit"]["measStamp"], dataH)
            if RMS:
                rms = self.getRMS(
                    self.acq["AcquisitionOrbit"]["channelNamesH"],
                    dataH.T,
                    plane="H",
                    excludeBPM=excludeBPM,
                )
                ax[0].plot(
                    self.acq["AcquisitionOrbit"]["measStamp"],
                    rms,
                    "-k",
                    label="RMS",
                )
                ax[0].legend()

            ax[0].set_xlabel("Acquisition [ms]")
            ax[0].set_ylabel("H [mm]")
            ax[0].set_title(
                self.name
                + " "
                + str(self.acq_info["AcquisitionOrbit"]["acqStamp"])
            )

            dataV = self.acq["AcquisitionOrbit"]["positionV"][:, :].T

            ax[1].plot(self.acq["AcquisitionOrbit"]["measStamp"], dataV)
            if RMS:
                rms = self.getRMS(
                    self.acq["AcquisitionOrbit"]["channelNamesV"],
                    dataV.T,
                    plane="V",
                    excludeBPM=excludeBPM,
                )
                ax[1].plot(
                    self.acq["AcquisitionOrbit"]["measStamp"],
                    rms,
                    "-k",
                    label="RMS",
                )
                ax[1].legend()

            ax[1].set_xlabel("Acquisition [ms]")
            ax[1].set_ylabel("V [mm]")

            plt.figure(figName).tight_layout()

            plt.pause(0.01)

    def plotTrajectoryEvolution(self, bpm_name=None, RMS=False, excludeBPM=[]):
        if bpm_name:
            try:
                if bpm_name[-1] == "H":
                    bpm_index = np.where(
                        self.acq["AcquisitionTrajectoryBBB"]["channelNamesH"]
                        == bpm_name
                    )[0][0]
                elif bpm_name[-1] == "V":
                    bpm_index = np.where(
                        self.acq["AcquisitionTrajectoryBBB"]["channelNamesV"]
                        == bpm_name
                    )[0][0]

                figName = (
                    "Trajectory evolution for " + self.name + " at " + bpm_name
                )
                if not plt.fignum_exists(figName):
                    plt.subplots(
                        ncols=1, nrows=1, figsize=(10, 3), num=figName
                    )

                ax = plt.figure(figName).axes

                ax[0].clear()

                ax[0].plot(
                    np.arange(
                        len(self.acq["AcquisitionTrajectoryBBB"]["bucket_idx"])
                    ),
                    self.acq["AcquisitionTrajectoryBBB"][
                        "position" + bpm_name[-1]
                    ][
                        bpm_index,
                        self.acq["AcquisitionTrajectoryBBB"]["bucket_idx"],
                    ],
                )

                ax[0].set_xlabel("Turn number [#]")
                ax[0].set_ylabel(bpm_name[-1] + " [mm]")
                ax[0].set_title(
                    bpm_name
                    + " "
                    + str(
                        self.acq_info["AcquisitionTrajectoryBBB"]["acqStamp"]
                    )
                )
                plt.figure(figName).tight_layout()
                plt.pause(0.01)

            except:
                logging.warning("BPM name incorrect or not found!")
        else:
            figName = "Trajectory evolution bunch in bucket " + str(
                self.acq["AcquisitionTrajectoryBBB"]["bunch_idx"]
            )
            if not plt.fignum_exists(figName):
                plt.subplots(ncols=1, nrows=2, figsize=(8, 6), num=figName)

            ax = plt.figure(figName).axes

            ax[0].clear()
            ax[1].clear()

            dataH = self.acq["AcquisitionTrajectoryBBB"]["positionH"][
                :, self.acq["AcquisitionTrajectoryBBB"]["bucket_idx"]
            ].T

            ax[0].plot(
                np.arange(
                    len(self.acq["AcquisitionTrajectoryBBB"]["bucket_idx"])
                ),
                dataH,
            )
            if RMS:
                rms = self.getRMS(
                    self.acq["AcquisitionTrajectoryBBB"]["channelNamesH"],
                    dataH.T,
                    plane="H",
                    excludeBPM=excludeBPM,
                )
                ax[0].plot(np.arange(len(dataH)), rms, "-k", label="RMS")
                ax[0].legend()

            ax[0].set_xlabel("Turn number [#]")
            ax[0].set_ylabel("H [mm]")
            ax[0].set_title(
                self.name
                + " bunch in bucket"
                + str(self.acq["AcquisitionTrajectoryBBB"]["bunch_idx"])
                + " "
                + str(self.acq_info["AcquisitionTrajectoryBBB"]["acqStamp"])
            )

            dataV = self.acq["AcquisitionTrajectoryBBB"]["positionV"][
                :, self.acq["AcquisitionTrajectoryBBB"]["bucket_idx"]
            ].T

            ax[1].plot(
                np.arange(
                    len(self.acq["AcquisitionTrajectoryBBB"]["bucket_idx"])
                ),
                dataV,
            )
            if RMS:
                rms = self.getRMS(
                    self.acq["AcquisitionTrajectoryBBB"]["channelNamesV"],
                    dataV.T,
                    plane="V",
                    excludeBPM=excludeBPM,
                )
                ax[1].plot(np.arange(len(dataV)), rms, "-k", label="RMS")
                ax[1].legend()

            ax[1].set_xlabel("Turn number [#]")
            ax[1].set_ylabel("V [mm]")

            plt.figure(figName).tight_layout()

            plt.pause(0.01)


class BPMBT(Device):
    def __init__(self, japc, bpm_name, plane="x", override_selector=None):
        super().__init__(japc, bpm_name, override_selector)

        self.bpm_name = self.name
        logging.debug("BPM class contructor")
        if "BT" in self.bpm_name:
            self.acq_field = "/Acquisition#position"

            self.plane = 0 if plane == "x" else 1
            logging.info("Field and plan set correctly")
        else:
            logging.error("BPM: Something went wrong!")

        self.label = rf"${plane}_{{{self.bpm_name}}}$ / mm"

    def getParameter(self, getHeader=True, **kwrds):
        # TODO: Check the unitis returned => it should be mm
        self.data_all, self.info = self.japc.getParam(
            self.bpm_name + self.acq_field, getHeader=getHeader, **kwrds
        )
        logging.info(f"data from BPM: {self.data_all[self.plane]}")
        return self.data_all, self.info

    def getPosition(self, getHeader=True, **kwrds):
        # TODO: Check the unitis returned => it should be mm
        self.data_all, self.info = self.japc.getParam(
            self.bpm_name + self.acq_field, getHeader=getHeader, **kwrds
        )
        logging.info(f"data from BPM: {self.data_all[self.plane]}")
        return self.data_all[self.plane], self.info


class BPMBPI(Device):
    def __init__(
        self, japc, bpm_name, bpm_type, plane="x", timingSelectorOverride=None
    ):
        super().__init__(japc, bpm_name, timingSelectorOverride)

        self.bpm_type = bpm_type
        self.bpm_name = self.name

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )

        logging.debug("BPM class contructor")
        self.acq_field = "/Acquisition#position"
        self.ch_names_prop = "/Acquisition#channelNames"
        self.plane = 0 if plane == "x" else 1

        self.label = rf"${plane}_{{{self.bpm_name}}}$ / mm"

        self.channels = self._get_channels(self.bpm_type)
        if self.plane == 0:
            self.mask = ["H" in ele for ele in self.channels]
        else:
            self.mask = ["V" in ele for ele in self.channels]

    def _get_channels(self, bpm_type):
        return self.japc.getParam(
            bpm_type + self.ch_names_prop, timingSelectorOverride=self.selector
        )

    def getParameter(self, getHeader=True, **kwargs):
        info = {}

        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )

        if "getHeader" in kwargs:
            getHeader = kwargs["getHeader"]
            del kwargs["getHeader"]

        data = self.japc.getParam(
            self.bpm_type + self.acq_field,
            timingSelectorOverride=self.selector,
            getHeader=getHeader,
        )

        positions, info = data if getHeader else (data, info)
        data = {
            key: value
            for key, value in zip(
                self.channels[self.mask], positions[self.mask]
            )
        }

        return (data, info) if getHeader else data

    def getPosition(self, getHeader=True, **kwrds):
        # TODO: Check the unitis returned => it should be mm
        data, info = self.getParameter(getHeader=True)
        pos = data[self.name]
        logging.info(f"data from BPM: {pos}")
        return pos, self.info


class BPMLHC(Device):
    def __init__(
        self, japc, bpm_name=None, acq_time=0, timingSelectorOverride=None
    ):
        super().__init__(japc, "lhc_bpm", timingSelectorOverride)

        self.bpm_name = bpm_name

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )

        self.device_name = "BPLOFSBA5"
        self.delay_var_name = "CAPSTART-BPL50S_FESA3/Delay#delay"

        self.acq_field = "GetCapData"
        self.set_field = "Setting"

        self.bpm_names = self._get_channels()
        print(self.bpm_names)
        if bpm_name is not None:
            assert self.bpm_name in self.bpm_names
            self.bpm_index = np.where(self.bpm_names == self.bpm_name)
            self.bpm_index = self.bpm_index[0][0]
        else:
            self.bpm_index = None

        self._n_bunches_string = "1-500"
        self._n_turns = 300

        self._delay_offset = 19

        self.h_field, self.v_field = "horPosition", "verPosition"
        self.n_bunches_field = "nbOfCapBunches"
        self.n_turns_field = "CaptureTurns"
        self.data_vars = (
            self.device_name + "/" + self.acq_field + "#" + self.h_field,
            self.device_name + "/" + self.acq_field + "#" + self.v_field,
        )

    @property
    def n_bunches(self):
        self._n_bunches = self.japc.getParam(
            self.device_name
            + "/"
            + self.acq_field
            + "#"
            + self.n_bunches_field,
            timingSelectorOverride=self.selector,
        )
        return self._n_bunches

    @property
    def n_bunches_string(self):
        self._n_bunches_string = self.japc.getParam(
            self.device_name + "/" + self.set_field + "#CaptureBunchString",
            timingSelectorOverride=self.selector,
        )
        return self._n_bunches_strin

    @n_bunches_string.setter
    def n_bunches_string(self, new_value):
        self._n_bunches_string = new_value
        self.japc.setParam(
            new_value,
            self.device_name + "/" + self.set_field + "#CaptureBunchString",
            timingSelectorOverride=self.selector,
        )

    @property
    def n_turns(self):
        self._n_turns = self.japc.getParam(
            self.device_name + "/" + self.set_field + "#" + self.n_turns_field,
            timingSelectorOverride=self.selector,
        )
        return self._n_turns

    @n_turns.setter
    def n_turns(self, new_value):
        self._n_turns = new_value
        self.japc.setParam(
            new_value,
            self.device_name + "/" + self.set_field + "#CaptureTurns",
            timingSelectorOverride=self.selector,
        )

    @property
    def acq_time(self):
        self._acq_time = self.japc.getParam(
            self.delay_var_name, timingSelectorOverride=self.selector
        )
        return self._acq_time - self._delay_offset

    @acq_time.setter
    def acq_time(self, new_value):
        self._acq_time = new_value
        self.japc.setParam(
            new_value + self._delay_offset,
            self.delay_var_name,
            timingSelectorOverride=self.selector,
        )

    def _get_channels(self):
        return self.japc.getParam(
            self.device_name + "/" + self.acq_field + "#bpmNames",
            timingSelectorOverride=self.selector,
        )

    def clean_data(self, data_prop, extract_single_bpm=False):
        data_raw = data_prop[self.h_field], data_prop[self.v_field]
        n_bunches = data_prop[self.n_bunches_field]
        n_turns = data_prop[self.n_turns_field]

        data = [
            data_.reshape((len(self.bpm_names), n_turns, n_bunches))
            for data_ in data_raw
        ]
        if extract_single_bpm:
            data = {
                key: value[self.bpm_index, :, :]
                for key, value in zip(["hor", "ver"], data)
            }
        else:
            data = {key: value for key, value in zip(["hor", "ver"], data)}
        return data

    def _clean_data_get(self, data_raw, extract_single_bpm=False):
        print(data_raw[0].shape)
        data = [
            data_.reshape((len(self.bpm_names), self.n_turns, self.n_bunches))
            for data_ in data_raw
        ]
        print(data[0].shape)
        if extract_single_bpm:
            data = {
                key: value[self.bpm_index, :, :]
                for key, value in zip(["hor", "ver"], data)
            }
        else:
            data = {key: value for key, value in zip(["hor", "ver"], data)}
        return data

    def getParameter(self, extract_single_bpm=False, getHeader=True, **kwargs):
        info = {}

        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )

        if "getHeader" in kwargs:
            getHeader = kwargs["getHeader"]
            del kwargs["getHeader"]

        data = self.japc.getParam(
            self.data_vars,
            timingSelectorOverride=self.selector,
            getHeader=getHeader,
        )
        data, info = data if getHeader else (data, info)
        data = self._clean_data_get(data, extract_single_bpm)

        return (data, info) if getHeader else data
    

    #Class method to extract unprocessed data from 3 BPMs in SPS, not only relevant bunches  
    def extract_all_bunches(self):
        bpm_pos_now = self.getParameter()
        all_bpm_pos_now = []
        for i in range(len(self.bpm_names)):   #iterate over all BPMs 
            all_bpm_pos_now.append(bpm_pos_now[0]['hor'][i])  #horizontal position of each BPM 
        return all_bpm_pos_now
        
        
    #Class method to extract data from 3 BPMs in SPS, providing data of the relevant bunches at injection and circulation to plot oscillations
    def extract_useful_bunches(self, bunches_per_batch = 1):
        bpm_pos_now = self.getParameter()
        bpm_pos_injected = []
        bpm_pos_circulating = []
        bunch_index_width = bunches_per_batch
        return_data = False  #boolean to determine whether to return BPM data or not 
        # -------- extract the injected and circulating beam to plot injection oscillations ----- 
        for i in range(len(self.bpm_names)):   #iterate over all BPMs 
            df = pd.DataFrame(
                bpm_pos_now[0]['hor'][i]  #horizontal position of each BPM 
            )
            df = df.loc[
                :, (df != 0).any(axis=0)
            ]  # remove all zero-valued columns, where there is no injected nor circulating beam
            if df.empty:
                logging.warning("\nEmpty BPM positions, waiting...\n")
            else:
                if (
                    len(df.columns) == 1
                    or df.astype(bool).sum(axis=0).iloc[0] != self.n_turns
                   ):
                    logging.warning(
                        "\nInjected or circulating beam lost...\n"
                    )
                else:
                    return_data = True  #proper BPM data, can be returned 
                    waveslice = np.array(
                        df.iloc[0, :]
                    )  # projection of the waveforms, first row
                    
                    # Find the indices of correct rows corresponding to the injected and circulating beam
                    if bunch_index_width == 1:
                        x1 = np.array(df.iloc[:, 0])
                        x2 = np.array(df.iloc[:, 1])
                    else:
                        indices = waveslice.nonzero()[
                            0
                        ]  # find indices of non-zero elements
                        ind_start = indices[0]
                        ind1 = (
                            ind_start + bunch_index_width
                        )  # if waveform index width is know, pick the last one
                        ind2 = indices[np.where(indices > ind1)][
                        0
                        ]  # second index simply the next value of "indices", larger than ind1
                        # bpm position vectors for injected and circulating
                        x1 = np.array(df.iloc[:, ind1])
                        x2 = np.array(df.iloc[:, ind2])
                    
                    #Append the injected and circulating BPM positions for each BPM 
                    bpm_pos_injected.append(x1)
                    bpm_pos_circulating.append(x2)
        
        #Return the BPM positions if non-zero or data is complete
        if return_data:
            return bpm_pos_injected, bpm_pos_circulating
        else:
            return [0],[0]


class BPMALPS(Device):
    def __init__(self, japc, sextant: int, timingSelectorOverride: str = None):
        name = f"BPM ALPS {sextant}"
        super().__init__(japc, name, timingSelectorOverride)

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.data_received = False
        self.sextant = sextant

        self.orbit_value = None

        self.orbit_parameterName = f"BPMALPS_{self.sextant}/Orbit"
        self.trajectory_parameterName = (
            f"BPMALPS_{self.sextant}/InjectionTrajectoryAcquisition"
        )

        self.channel_names = None
        self.mask_h, self.mask_v = None, None

    def getParameter(self, acquisition="orbit", getHeader=True, **kwargs):
        info = {}

        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )

        if "getHeader" in kwargs:
            getHeader = kwargs["getHeader"]
            del kwargs["getHeader"]

        if acquisition == "orbit":
            data = self.japc.getParam(
                self.orbit_parameterName,
                timingSelectorOverride=self.selector,
                getHeader=getHeader,
            )
            data, info = data if getHeader else (data, info)
            data = self.analyse_orbit_data(data)

        elif "trajectory" in acquisition:
            data = self.japc.getParam(
                self.trajectory_parameterName,
                timingSelectorOverride=self.selector,
                getHeader=getHeader,
            )
            try:
                injection_event = int(acquisition[-1])
            except ValueError:
                injection_event = 1
            data, info = data if getHeader else (data, info)
            data = self.analyse_trajectory_data(
                data, injection_event=injection_event
            )

        elif "traj-orbit" in acquisition:
            data = self.japc.getParam(
                self.orbit_parameterName,
                timingSelectorOverride=self.selector,
            )
            data_orbit = self.analyse_orbit_data(data)
            data_t = self.japc.getParam(
                self.trajectory_parameterName,
                timingSelectorOverride=self.selector,
                getHeader=getHeader,
            )
            data, info = data_t if getHeader else (data_t, info)
            try:
                injection_event = int(acquisition[-1])
            except ValueError:
                injection_event = 1
            data_traj = self.analyse_trajectory_data(
                data, injection_event=injection_event
            )
            data_x = data_traj[0] - data_orbit[0][1, :]
            data_y = data_traj[1] - data_orbit[1][1, :]
            data = (data_x, data_y)

        data = {key: value for key, value in zip(["hor", "ver"], data)}

        return (data, info) if getHeader else data

    def traj_minus_co(
        self,
        data_property_co: Dict,
        data_property_traj: Dict,
        injection_event: int = 1,
    ):
        data_orbit = self.analyse_orbit_data(data_property_co)
        data_traj = self.analyse_trajectory_data(
            data_property_traj, injection_event=injection_event
        )
        try:
            data_x = data_traj[0] - data_orbit[0][1, :]
            data_y = data_traj[1] - data_orbit[1][1, :]
        except IndexError:
            print("Orbit data still missing")
            return None, None
        return data_x, data_y

    def _set_masks(self, data_property: Dict):
        self.channel_names = data_property["channelNames"]
        self.mask_h = ["BPH" in ele for ele in self.channel_names]
        self.mask_v = ["BPV" in ele for ele in self.channel_names]

    def _split_planes(
        self, orbit_raw: np.ndarray
    ) -> Tuple[np.ndarray, np.ndarray]:
        return orbit_raw[:, self.mask_h], orbit_raw[:, self.mask_v]

    def analyse_trajectory_data(
        self, data_property: Dict, injection_event: int = 1
    ) -> Tuple[np.ndarray, np.ndarray]:
        self._set_masks(data_property)
        traj_data_raw = np.array(
            data_property[f"injectionTrajectory{injection_event}"]
        )
        self.turns_trajecotry = np.arange(traj_data_raw.shape[-1])
        self.traj_data_raw = traj_data_raw.transpose()
        self.data_h, self.data_v = self._split_planes(self.traj_data_raw)
        return self.data_h, self.data_v

    def analyse_orbit_data(
        self, data_property: Dict
    ) -> Tuple[np.ndarray, np.ndarray]:
        self._set_masks(data_property)
        orbit_data_raw = np.array(data_property["positions"])
        self.times = data_property["measStamp"]
        self.orbit_data_raw = orbit_data_raw.transpose()
        self.data_h, self.data_v = self._split_planes(self.orbit_data_raw)
        return self.data_h, self.data_v

    def bpm_name(self, plane):
        mask = self.mask_h if "h" in plane else self.mask_v
        if self.channel_names is not None:
            return self.channel_names[mask]
        else:
            return None

    def get_mrp_cycle(
        self,
        data_h: np.ndarray,
        time_cycle: np.ndarray,
        end_acquistion: int = -1,
    ) -> Tuple[np.ndarray, np.ndarray]:
        if end_acquistion < 0:
            return time_cycle, np.nanmean(data_h, axis=1)
        else:
            mask_time = time_cycle <= end_acquistion
            return time_cycle[mask_time], np.nanmean(data_h[mask_time], axis=1)
