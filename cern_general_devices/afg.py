import logging
import typing
import abc
from cern_general_devices import Device
import vxi11

from ._version import version as __version__

class AFG(Device):
    def __init__(
            self, 
            ip_address: str,
            name: str = "AFG"
    ):
        """Generic SCPI interface to KeySight/Agilent Function Generators
        
        Args:
            ip_address (str): IP address of the device
            name (str): Name of the device
        """
        self.instrument = vxi11.Instrument(ip_address)
        self.name = name

    def print_name(self) -> None:
        """Function to print name"""
        print(self.name)

    def __str__(self) -> str:
        return self.name + " at " + self.instrument.host
    
    def write(self, command: str) -> None:
        """
        Wrapper around the native write method of vxi11
        
        Args:
            command (str): SCPI command to send to the device

        Returns:
            None
        """
        return self.instrument.write(command)
    
    def ask(self, command: str) -> str:
        """
        Wrapper around the native ask method of vxi11

        Args:
            command (str): SCPI command to send to the device

        Returns:
            str: Response from the device
        """
        return self.instrument.ask(command)
    
    def getParameter(self, command: str) -> str:
        """Wrapper around the ask method of vxi11, compatible with Device interface
        
        Args:
            command (str): SCPI command to send to the device
        
        Returns:
            str: Response from the device
        """
        qmark = "?" if command[-1] != "?" else ""
        return self.instrument.ask(command + qmark)

    def setParameter(self, command: str, value: str, unit: str = None) -> None:
        """Wrapper around the write method of vxi11, compatible with Device interface

        Args:
            command (str): SCPI command to send to the device
            value (str): Value to set the parameter to
            unit (str): Unit of the value (optional)

        Returns:
            None
        """
        self.instrument.write(command + " " + value + (" " + unit if unit is not None else ""))
        pass

    def configureRFKO(self, 
                    center_frequency: float, 
                    frequency_deviation: float,
                    repeition_rate: float,
                    voltage: float = None,
                    gain: float = None) -> None:
        """Configure the device for frequency modulation

        Args:
            center_frequency (float): Center frequency of the modulation [Hz]
            frequency_deviation (float): Frequency deviation of the modulation [Hz]
            repeition_rate (float): Repetition rate of the modulation [Hz]
            voltage (float): Voltage of the modulation [Vpp] (optional)
            gain (float): The Qmeter gain of the modulation (Gain 1 = 2 Vpp) (optional)
        """
        if voltage is None and gain is None:
            raise ValueError("Either voltage or gain must be specified")
        elif voltage is not None and gain is not None:
            raise ValueError("Only one of voltage or gain must be specified")
        elif voltage is not None and gain is None:
            voltage = voltage
        elif voltage is None and gain is not None:
            voltage = gain * 2

        # Set CH1 Shape, FM Source, CH2 Shape
        self.instrument.write(f"SOUR1:FUNC SIN")
        self.instrument.write(f"SOUR1:FM:SOUR CH2")
        self.instrument.write(f"SOUR2:FUNC RAMP")

        # Set CH1 Center, Deviation Freq and Volt
        self.instrument.write(f"SOUR1:FREQ {center_frequency} Hz")
        self.instrument.write(f"SOUR1:FM:DEV {frequency_deviation} Hz")
        self.instrument.write(f"SOUR1:VOLT {voltage} Vpp")

        # Set CH2 Frequency and Volt
        self.instrument.write(f"SOUR2:FREQ {repeition_rate} Hz")
        self.instrument.write(f"SOUR2:VOLT 1 Vpp")

        # Enable CH2
        self.instrument.write(f"OUTP2:STAT ON")

        # Enable CH1 FM
        self.instrument.write(f"SOUR1:FM:STAT ON")
        self.instrument.write(f"OUTP1:STAT ON")

    def setVoltage(self, voltage: float, channel: int = 1) -> None:
        """Set the voltage of a channel

        Args:
            voltage (float): Voltage to set [Vpp]
            channel (int): Channel to set (optional)
        """

        self.instrument.write(f"SOUR{str(channel)}:VOLT {voltage} Vpp")

    def setFrequency(self, frequency: float, channel: int = 1) -> None:
        """Set the frequency of a channel

        Args:
            frequency (float): Frequency to set [Hz]
            channel (int): Channel to set (optional, default = 1)
        """

        self.instrument.write(f"SOUR{str(channel)}:FREQ {frequency} Hz")

    def setFMDeviation(self, frequency_deviation: float, channel: int = 1) -> None:
        """Set the frequency deviation of the FM modulation

        Args:
            frequency_deviation (float): Frequency deviation to set [Hz]
            channel (int): Channel to set (optional, default = 1)
        """
        self.instrument.write(f"SOUR{str(channel)}:FM:DEV {frequency_deviation} Hz")

    def setRepRate(self, repeition_rate: float, channel: int = 2) -> None:
        """Set the repetition rate of the FM modulation

        Args:
            repeition_rate (float): Repetition rate to set [Hz]
            channel (int): Channel to set (optional, default = 2)
        """
        self.instrument.write(f"SOUR{str(channel)}:FREQ {repeition_rate} Hz")

    def enable(self, channel: int = 1) -> None:
        """Enable a channel

        Args:
            channel (int): Channel to enable (optional)
        """
        self.instrument.write(f"OUTP{str(channel)}:STAT ON")