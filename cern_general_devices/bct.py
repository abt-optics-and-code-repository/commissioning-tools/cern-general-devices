import numpy as np
from cern_general_devices import Device
import logging
import typing as t
from pyjapc import PyJapc


class BCTW(Device):
    def __init__(
        self,
        japc: t.Optional[PyJapc],
        timingSelectorOverride: t.Optional[str] = None,
    ) -> None:
        """
        Class that handles the selection of the first injected bucket
        Args:
            japc (PyJapc): Instance of PyJapc object
            timingSelectorOverride (str, optional): Timing selector override, for example: SPS.USER.SFTPRO2. Defaults to None.
        """

        super().__init__(japc, "bct", timingSelectorOverride)

        if japc is None:
            japc = PyJapc(noSet=True)
            logging.warning("No JAPC instance provided, creating a new one")
        self.japc = japc
        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc

        self.bct_var = "SPS.BCTW.31931/CaptureAcquisition#totalIntensity"

    def getParameter(self, ele_name=None, getHeader=True, **kwargs):  # type: ignore
        info = {}
        ele_name = self.bct_var if ele_name is None else ele_name
        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        data = (
            self.japc.getParam(ele_name, getHeader, **kwargs)
            if getHeader
            else (
                self.japc.getParam(ele_name, getHeader, **kwargs),
                info,
            )
        )
        return data


class BCTFI(Device):
    def __init__(
        self,
        japc: t.Optional[PyJapc],
        tl_name: str,
        timingSelectorOverride: t.Optional[str] = None,
    ) -> None:
        """
        Class to read TL fast BCTs
        Args:
            japc (PyJapc): Instance of PyJapc object
            timingSelectorOverride (str, optional): Timing selector override, for example: SPS.USER.SFTPRO2. Defaults to None.
        """

        super().__init__(japc, "bctfi", timingSelectorOverride)

        if japc is None:
            japc = PyJapc(noSet=True)
            logging.warning("No JAPC instance provided, creating a new one")
        self.japc = japc
        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.tl_name = tl_name

        self.mapping_tl_bct = {
            "TI2": "TI2.BCTFI.29125",
            "TT60": "TT60.BCTFI.610225",
            "TT40": "TT40.BCTFI.400344",
            "TI8": "TI8.BCTFI.87750",
            "TT10": "TT10.BCTFI.102834",
        }
        self.bct_name = self.mapping_tl_bct[self.tl_name.upper()]

        self.bct_var = f"{self.bct_name}/CaptureAcquisition"

    def getParameter(self, ele_name=None, getHeader=True, **kwargs):  # type: ignore
        ele_name = self.bct_var if ele_name is None else ele_name
        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        data = (
            self.japc.getParam(ele_name, getHeader, **kwargs)
            if getHeader
            else (
                self.japc.getParam(ele_name, getHeader, **kwargs),
                {},
            )
        )
        return data

    def extract_single_bunch(
        self, intensity_property: t.Dict[str, t.Any]
    ) -> float:
        intensity = intensity_property["totalIntensity"]
        exponent = intensity_property["totalIntensity_unitExponent"]
        try:
            intensity_tot = np.max(intensity) * 10 ** (exponent)
        except Exception as e:
            logging.warning("Exception in extracting single bunch")
            logging.warning(e)
            intensity_tot = 0
        return intensity_tot

    def get_single_bunch(self, **kwargs: t.Dict[t.Any, t.Any]) -> float:
        if self.japc is None:
            raise ValueError("No JAPC instance provided")
        data = self.japc.getParam(self.bct_var, **kwargs)
        return np.max(data["totalIntensity"]) * 10 ** (
            data["totalIntensity_unitExponent"]
        )


def moment(
    x: np.ndarray, fx: np.ndarray, avg: float, mom: float = 2.0
) -> np.ndarray:
    return np.sum(fx * (x - avg) ** mom) / np.sum(fx)


class BCTDC(Device):
    def __init__(
        self,
        japc: t.Optional[PyJapc],
        timingSelectorOverride: t.Optional[str] = None,
    ) -> None:
        super().__init__(japc, "bctdc", timingSelectorOverride)

        if japc is None:
            japc = PyJapc(noSet=True)
            logging.warning("No JAPC instance provided, creating a new one")
        self.japc = japc
        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.validCycle = False
        self.bct5_var = "SPS.BCTDC.51454"

        self._set_variables()

    def _set_variables(self) -> None:
        self.intensity_var = "/Acquisition#totalIntensity"
        if self.japc is None:
            raise ValueError("No JAPC instance provided")
        self.ts = self.japc.getParam(
            self.bct5_var + "/Acquisition#samplingTime",
            timingSelectorOverride=self.selector,
        )
        self.exponent = self.japc.getParam(
            self.bct5_var + "/Acquisition#totalIntensity_unitExponent",
            timingSelectorOverride=self.selector,
        )
        self.dump_cycle_time = None
        self.time = None
        self.ft_intensity_var = self.bct5_var + "/Acquisition#sbfIntensity"

        self.bct5_sx_extracted_intensity = (
            self.bct5_var + "/Acquisition#slowExtInt"
        )
        self.bct5_dumped_intensity_var = (
            self.bct5_var + "/Acquisition#dumpInt"
        )
        self.bct5_dump_time_var = self.bct5_var + "/Acquisition#dumpCycleTime"
        self.bct5_extracted_intensity = (
            self.bct5_var + "/Acquisition#intensityExtracted"
        )

    def getParameter(self, getHeader=True, **kwargs):  # type: ignore
        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        data_temp = (
            self.japc.getParam(
                self.bct5_var + self.intensity_var, getHeader, **kwargs
            )
            if getHeader
            else (
                self.japc.getParam(
                    self.bct5_var + self.intensity_var, getHeader, **kwargs
                ),
                {},
            )
        )
        return self.extract_data(data_temp[0]), data_temp[1]

    def extract_data(self, data_bct: t.Sequence[float]) -> np.ndarray:
        data_bct = np.array(data_bct).astype("float64")
        print("bct")
        print(data_bct)
        self.time = np.linspace(
            0, (len(data_bct) - 1) * self.ts, len(data_bct)
        )
        data = np.zeros((2, len(self.time)))  # type: ignore
        data[0, :] = self.time
        data[1, :] = data_bct * 10 ** (self.exponent)
        return data

    def get_flat_top_intensity(self) -> float:
        if self.japc is None:
            raise ValueError("No JAPC instance provided")
        ft_intensity = self.japc.getParam(
            self.ft_intensity_var,
            timingSelectorOverride=self.selector,
        )
        return ft_intensity * 10 ** (self.exponent)

    def convert_in_protons(self, intensity: float) -> float:
        return intensity * 10 ** (self.exponent)

    def calculate_dumped_intensity(
        self, data_dumped_intensity: float
    ) -> float:
        return self.convert_in_protons(data_dumped_intensity)

    def get_dumped_intensity(self) -> float:
        if self.japc is None:
            raise ValueError("No JAPC instance provided")
        dumped_intensity, self.dump_cycle_time = self.japc.getParam(
            [
                self.bct5_var + "/Acquisition#dumpInt",
                self.bct5_var + "/Acquisition#dumpCycleTime",
            ],
            timingSelectorOverride=self.selector,
        )
        return dumped_intensity * 10 ** (self.exponent)

    def get_extracted_intensity(
        self, getHeader: bool = False
    ) -> t.Union[t.Tuple[float, t.Dict[str, t.Any]], float]:
        if self.japc is None:
            raise ValueError("No JAPC instance provided")
        extracted_intensity, info = self.japc.getParam(
            self.bct5_var + "/Acquisition#slowExtInt",
            timingSelectorOverride=self.selector,
            getHeader=True,
        )
        extracted_intensity = extracted_intensity * 10 ** (self.exponent)
        dumped_intensity = self.get_dumped_intensity()
        print(extracted_intensity, dumped_intensity)
        tot_extracted_intensity = extracted_intensity - dumped_intensity
        if getHeader:
            return (
                (
                    tot_extracted_intensity
                    if tot_extracted_intensity >= 0
                    else 0
                ),
                info,
            )
        else:
            return (
                tot_extracted_intensity if tot_extracted_intensity >= 0 else 0
            )

    def calculate_extracted_intensity(
        self, extracted_intensity: float, dumped_intensity: float
    ) -> float:
        extracted_intensity = extracted_intensity * 10 ** (self.exponent)
        dumped_intensity = dumped_intensity * 10 ** (self.exponent)
        tot_extracted_intensity = extracted_intensity - dumped_intensity

        return tot_extracted_intensity if tot_extracted_intensity >= 0 else 0

    def get_momentum_spread(
        self, data_bct: np.ndarray
    ) -> t.Tuple[t.Tuple[np.ndarray, np.ndarray], float, float]:
        self.time = data_bct[0, :]
        self.int_vec = data_bct[1, :]

        self.dpp_x, self.dpp_y = self.calculate_mom_spread()
        return (self.dpp_x, self.dpp_y), self.dpp_mean, self.dpp_rms

    def calculate_mom_spread(self) -> t.Tuple[np.ndarray, np.ndarray]:
        # Get tune from LSA
        # calculate tune range
        # maybe do a fit
        # save pickles

        # tune_lsa = self.japc.getParam('rmi://virtual_sps/SPSBEAM/QH')
        if self.japc is None:
            raise ValueError("No JAPC instance provided")
        tune_lsa = self.japc.getParam("rmi://virtual_sps/SPSBEAM/MOMENTUM")
        beam_out = self.japc.getParam(
            "SX.BEAM-OUT-CTML/ControlValue#controlValue"
        )
        beam_in = self.japc.getParam("SIX.MC-CTML/ControlValue#controlValue")
        flat_top = self.japc.getParam(
            "SX.S-FTOP-CTML/ControlValue#controlValue"
        )

        ft_time = [flat_top, beam_out]
        ft_index = [
            list(tune_lsa[0]).index(ft_time[0]) + 2,
            list(tune_lsa[0]).index(ft_time[1]),
        ]

        delta_tune = tune_lsa[1][ft_index[1]] - tune_lsa[1][ft_index[0]]

        self.tune_time = tune_lsa[0][ft_index[0] : ft_index[1]] - beam_in
        self.tune = tune_lsa[1][ft_index[0] : ft_index[1]]

        self.time_int = np.linspace(
            flat_top - beam_in, beam_out - beam_in, 200
        )

        self.bct_int = np.interp(self.time_int, self.time, self.int_vec)

        inv_bct = self.bct_int[0] - self.bct_int
        didt = np.gradient(inv_bct, 5e-3)

        self.tune_int = np.interp(self.time_int, self.tune_time, self.tune)

        dtune = np.linspace(
            -1 * delta_tune / 2,
            delta_tune / 2,
            len(self.time_int),
            endpoint=True,
        )
        # dpp = dtune / (self.chroma * 26.66) * 1e3
        dpp = dtune / 400.0 * 1e3
        dq_dt = np.gradient(self.tune_int, self.time_int)
        mask = (dq_dt > 1e-6) & (dq_dt < 1e6)

        self.pdf_dpp = didt[mask] / dq_dt[mask]
        #            self.pdf_dpp = self.tune_int

        #            self.pdf_dpp = self.pdf_dpp / max(self.pdf_dpp)

        self.dpp_mean = np.average(dpp[mask], weights=self.pdf_dpp)
        self.dpp_rms = moment(dpp[mask], self.pdf_dpp, self.dpp_mean)

        return dpp[mask], self.pdf_dpp


class BCTDC24(BCTDC):
    def __init__(
        self,
        japc: t.Optional[PyJapc],
        timingSelectorOverride: t.Optional[str] = None,
    ) -> None:
        super().__init__(japc, timingSelectorOverride)
        self.bct5_var = "SPS.BCTDC24.51454"

        self._set_variables()
