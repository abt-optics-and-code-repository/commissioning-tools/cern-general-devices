import numpy as np
from cern_general_devices import Device
import logging


class BucketSelector(Device):
    def __init__(self, japc, timingSelectorOverride=None):
        """
        Class that handles the selection of the first injected bucket
        Args:
            japc (PyJapc): Instance of PyJapc object
            timingSelectorOverride (str, optional): Timing selector override, for example: SPS.USER.SFTPRO2. Defaults to None.
        """

        super().__init__(japc, "early_dump", timingSelectorOverride)

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.data = []

        self.bucket_var_prop = "SPSMasterFrfInj/RF1TriggerUnit2"
        self.bucket_var_field = "bOffset"
        self.template = self.japc.getParam(
            self.bucket_var_prop,
            timingSelectorOverride=self.selector,
            getHeader=False,
        )
        for key in self.template.keys():
            if key != "enable":
                self.template[key] = self.template[key].astype(int)

        self.vars = [
            self.bucket_var_prop + "#" + self.bucket_var_field,
        ]

        self.injection_to_set = 0
        self.bucket_to_set = 0

        self.min_bucket = 0
        self.max_bucket = 4600

    def setParameter(self, injection_to_set, value_to_set, **kwargs):

        if self.are_limits_ok(value_to_set):
            self._set_with_no_check(
                injection_to_set,
                value_to_set,
                timingSelectorOverride=self.selector,
                **kwargs
            )
            self.bucket_to_set = value_to_set
        else:
            logging.warning("Set not done, out of limits!")

    def check_set(self, bucket_set):
        return bucket_set == self.delay_to_set

    def are_limits_ok(self, set_value):
        return self.min_bucket <= set_value <= self.max_bucket

    def _set_with_no_check(self, injection_to_set, bucket_to_set, **kwargs):
        value_temp = self.template[self.bucket_var_field][:]
        value_temp[injection_to_set] = bucket_to_set

        print(value_temp)
        protpery_to_set = self.template.copy()
        protpery_to_set[self.bucket_var_field] = value_temp

        self.japc.setParam(self.bucket_var_prop, protpery_to_set, **kwargs)

    def getParameter(self, ele_name=None, getHeader=True, **kwargs):
        info = {}
        ele_name = self.vars if ele_name is None else ele_name
        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        data = (
            self.japc.getParam(ele_name, getHeader, **kwargs)
            if getHeader
            else (
                self.japc.getParam(ele_name, getHeader, **kwargs),
                info,
            )
        )
        return data


# import pyjapc

# japc = pyjapc.PyJapc("SPS.USER.SFTPRO1")
# bs = BucketSelector(japc)
# test = bs.getParameter()
# bs.setParameter(0, 1)
