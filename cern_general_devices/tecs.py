import numpy as np
import datetime
from cern_general_devices import Device
import logging
from collections import deque
from cernml import coi, japc_utils
import typing as t
import numpy.typing as npt
import pyjapc

logger = logging.getLogger()
logger.setLevel("INFO")


class TECS(Device):
    def __init__(
        self,
        japc: pyjapc.PyJapc,
        typeScan: t.Optional[str] = None,
        timingSelectorOverride: t.Optional[str] = None,
    ) -> None:
        super().__init__(japc, "TECS", timingSelectorOverride)

        if japc is None:
            japc = pyjapc.PyJapc(noSet=True)
            logging.warning("No JAPC instance provided, creating a new one")
        self.japc = japc
        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.isCrystalReady = False

        self.type = typeScan

        self.crystalStting = "TECS.21602/Setting"
        self.crystalAcquisition = "TECS.21602/PPMAcquisition"
        self.crystalAcquisitionAll = "TECS.21602/Acquisition"

        self.demandedAngle = 0.0
        self.demandedPos = 0.0

        self.min_angle_step = 8.0
        self.min_position_step = 10e-3

        self.modeAngle = "total"

        self.crystInfo: t.List[t.Deque] = [deque(maxlen=50) for i in range(5)]

        self.pos_limits = [0, 80]
        self.angle_limits = [-100000, 100000]
        self.all_vars_non_ppm = [
            self.crystalAcquisition + "#crystalPosition",
            self.crystalAcquisition + "#crystalAngle",
            self.crystalAcquisition + "#crystalPositionCTRL",
            self.crystalAcquisition + "#crystalAngleCTRL",
        ]

        self.all_vars = [
            self.crystalAcquisition + "#crystalPosition",
            self.crystalAcquisition + "#crystalAngle",
            self.crystalAcquisition + "#crystalPositionCTRL",
            self.crystalAcquisition + "#crystalAngleCTRL",
        ]

    def getCrystInfo(self) -> None:
        self.get_all_crystal_para()

        self.crystInfo[0].append(datetime.datetime.now())
        self.crystInfo[1].append(self.crystalPositionCTRL)
        self.crystInfo[2].append(self.crystalAngleCTRL)
        self.crystInfo[3].append(self.crystalPosition)
        self.crystInfo[4].append(self.crystalAngle)

    def _set_both(self, pos: float, angle: float) -> None:
        # in mm, urad

        dic_to_set = {"angle": angle, "position": pos}

        if self.japc is not None:
            self.japc.setParam(
                self.crystalStting, dic_to_set, timingSelectorOverride=""
            )

        self.demandedPos = pos
        self.demandedAngle = angle

    def get_all_crystal_para(self) -> t.Tuple[float, float, float, float]:
        if self.japc is not None:
            all_data = self.japc.getParam(self.all_vars)
        logging.info(f"data = {all_data}")
        (
            self.crystalPosition,
            self.crystalAngle,
            self.crystalPositionCTRL,
            self.crystalAngleCTRL,
        ) = all_data
        return all_data

    def check_pos(self, position: float) -> bool:
        if np.abs(self.crystalPositionCTRL - position) <= 50e-3:
            return True
        else:
            return False

    def check_angle(self, angle: float) -> bool:
        if np.abs(self.crystalAngleCTRL - angle) <= 3.0:
            return True
        else:
            return False

    def is_tecs_ready(self, demanded_settings: t.Sequence[float]) -> bool:
        self.get_all_crystal_para()
        position, angle = demanded_settings
        return self.check_pos(position) and self.check_angle(angle)

    def fix_settings(self, settings: t.Sequence[float]) -> t.Sequence[float]:
        self.get_all_crystal_para()
        delta_pos = settings[0] - self.crystalPositionCTRL
        delta_angle = settings[1] - self.crystalAngleCTRL
        logger.info(
            f"Delta pos requested: {delta_pos}, Delta angle: {delta_angle}"
        )
        delta_pos_fixed = (
            0.0
            if np.abs(delta_pos) <= self.min_position_step
            else np.abs(delta_pos)
        )
        delta_angle_fixed = (
            0.0
            if np.abs(delta_angle) <= self.min_angle_step
            else np.abs(delta_angle)
        )
        logger.info(f"Delta fix pos: {delta_pos}, Delta angle: {delta_angle}")
        return [
            self.crystalPositionCTRL + np.sign(delta_pos) * delta_pos_fixed,
            self.crystalAngleCTRL + np.sign(delta_angle) * delta_angle_fixed,
        ]

    def checkReadiness(self) -> None:
        # check scan type
        # check LVDT vs controller vs settings
        self.get_all_crystal_para()
        if self.type == "position":
            print("position")
            print(
                self.crystalPosition,
                self.crystalPositionCTRL,
                self.demandedPos,
            )
            if np.abs(self.crystalPositionCTRL - self.demandedPos) <= 50e-3:
                if (
                    np.abs(self.crystalPosition - self.crystalPositionCTRL)
                    * 0
                    <= 1e10
                ):
                    self.isCrystalReady = True
                else:
                    self.isCrystalReady = False
            else:
                self.isCrystalReady = False
        elif self.type == "angle":
            print("angle")
            print(
                self.crystalAngle,
                self.crystalAngleCTRL,
                self.demandedAngle,
            )
            if np.abs(self.crystalAngleCTRL - self.demandedAngle) <= 3.0:
                if (
                    np.abs(self.crystalAngle - self.crystalAngleCTRL) * 0
                    <= 1e10
                ):
                    self.isCrystalReady = True
                else:
                    self.isCrystalReady = False
            else:
                self.isCrystalReady = False

    def setParameter(  # type: ignore
        self, value_to_set: t.Sequence[float] = np.zeros(2), **kwargs: t.Any  # type: ignore
    ) -> None:  # type: ignore
        if self.are_limits_ok(
            value_to_set[0], self.pos_limits
        ) and self.are_limits_ok(value_to_set[1], self.angle_limits):
            self._set_both(value_to_set[0], value_to_set[1])
        else:
            logging.warning("Set not done, out of limits!")

    def are_limits_ok(
        self, set_value: float, limits: t.Sequence[float]
    ) -> bool:
        return limits[0] <= set_value <= limits[1]

    def getParameter(self, ele_name: t.Optional[t.List[str]] = None, getHeader: bool = True, **kwargs: t.Any):  # type: ignore
        ele_name = self.all_vars if ele_name is None else ele_name
        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        if "getHeader" in kwargs:
            getHeader = kwargs["getHeader"]
            del kwargs["getHeader"]

        if "only_ctrl" in kwargs:
            only_ctrl = kwargs.get("only_ctrl", False)
            del kwargs["only_ctrl"]
        else:
            only_ctrl = False

        ele_name = ele_name[-2:] if only_ctrl else ele_name

        if self.japc is not None:
            data = (
                self.japc.getParam(ele_name, getHeader, **kwargs)
                if getHeader
                else (
                    self.japc.getParam(ele_name, getHeader, **kwargs),
                    {},
                )
            )
            return data
        else:
            raise ValueError("No JAPC instance provided")


class TECA(Device):
    def __init__(
        self,
        japc: t.Optional[pyjapc.PyJapc] = None,
        typeScan: t.Optional[str] = None,
        timingSelectorOverride: t.Optional[str] = None,
        start_monitoring: bool = False,
    ) -> None:
        super().__init__(japc, "TECS", timingSelectorOverride)

        if japc is None:
            japc = pyjapc.PyJapc(noSet=True)
            logging.warning("No JAPC instance provided, creating a new one")
        self.japc = japc

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.isCrystalReady = False
        self.type = typeScan

        self.crystalStting = "TECA.41777/Setting"
        self.crystalAcquisition = "TECA.41777/PPMAcquisition"
        self.crystalAcquisitionAll = "TECA.41777/Acquisition"

        self.dw_lost_step_var = (
            "TECA.41777.DW/ExpertStatus#motionControllerErrors"
        )
        self.up_lost_step_var = (
            "TECA.41777.UP/ExpertStatus#motionControllerErrors"
        )
        self.demandedAngle = 0.0
        self.demandedPos = 0.0

        self.modeAngle = "total"

        self.crystInfo: t.List[t.Deque] = [deque(maxlen=50) for i in range(5)]

        self.lvdt_nonppm_variables = [
            self.crystalAcquisitionAll + "#crystalPosition",
            self.crystalAcquisitionAll + "#crystalAngle",
        ]
        self.ctrl_nonppm_variables = [
            self.crystalAcquisitionAll + "#crystalPositionCTRL",
            self.crystalAcquisitionAll + "#crystalAngleCTRL",
        ]
        self.lvdt_ppm_variables = [
            self.crystalAcquisition + "#crystalPosition",
            self.crystalAcquisition + "#crystalAngle",
        ]
        self.ctrl_ppm_variables = [
            self.crystalAcquisition + "#crystalPositionCTRL",
            self.crystalAcquisition + "#crystalAngleCTRL",
        ]
        self.all_vars_non_ppm = [
            *self.lvdt_nonppm_variables,
            *self.ctrl_nonppm_variables,
        ]

        self.all_vars = [*self.lvdt_ppm_variables, *self.ctrl_ppm_variables]

        self.position_lvdt_field = "position"
        self.angle_lvdt_field = "angle"
        self.position_ctrl_field = "positionCTRL"
        self.angle_ctrl_field = "angleCTRL"

        self.teca_hystory: t.Dict[str, deque] = dict(
            time=deque(maxlen=50),
            set_position=deque(maxlen=50),
            set_angle=deque(maxlen=50),
            measured_position=deque(maxlen=50),
            measured_angle=deque(maxlen=50),
            measured_position_ctrl=deque(maxlen=50),
            measured_angle_ctrl=deque(maxlen=50),
        )
        self.demanded_pos_lvdt: float = 0.0
        self.demanded_angle_lvdt: float = 0.0

        self.pos_limits = [0, 80]
        self.angle_limits = [-100000, 100000]

        self.min_position_step = 10e-3
        self.min_angle_step = 8.5

        self.min_step_allowed = np.array(
            [self.min_position_step, self.min_angle_step]
        )
        self.max_step_allowed = np.array([20, 2000])

        self.start_monitoring(start_monitoring)

    def start_monitoring(self, start_monitoring: bool) -> None:
        if start_monitoring:
            self.stream = japc_utils.subscribe_stream(
                self.japc, self.crystalAcquisitionAll, selector=self.selector
            )
            self.stream.start_monitoring()

    def getCrystInfo(self) -> None:
        self.get_all_crystal_para()

        self.crystInfo[0].append(datetime.datetime.now())
        self.crystInfo[1].append(self.crystalPositionCTRL)
        self.crystInfo[2].append(self.crystalAngleCTRL)
        self.crystInfo[3].append(self.crystalPosition)
        self.crystInfo[4].append(self.crystalAngle)

    def _set_both(self, pos: float, angle: float) -> None:
        # in mm, urad

        dic_to_set = {"angle": angle, "position": pos}

        if self.japc is not None:
            self.japc.setParam(
                self.crystalStting, dic_to_set, timingSelectorOverride=""
            )
        else:
            raise ValueError("No JAPC instance provided")

        self.demandedPos = pos
        self.demandedAngle = angle

    def get_lvdt_readings(
        self, timingSelectorOverride: str = ""
    ) -> np.ndarray:
        if self.japc is not None:
            return self.japc.getParam(
                self.lvdt_ppm_variables,
                timingSelectorOverride=timingSelectorOverride,
            )
        else:
            raise ValueError("No JAPC instance provided")

    def get_all_crystal_para(self) -> t.Tuple[float, float, float, float]:
        if self.japc is not None:
            all_data = self.japc.getParam(self.all_vars)
            logging.info(f"data = {all_data}")
            (
                self.crystalPosition,
                self.crystalAngle,
                self.crystalPositionCTRL,
                self.crystalAngleCTRL,
            ) = all_data
            return all_data
        else:
            raise ValueError("No JAPC instance provided")

    def _is_set_as_ctrl(self, measured_ctrl: np.ndarray) -> bool:
        is_pos_ok = (
            np.abs(measured_ctrl[0] - self.demandedPos)
            <= self.min_position_step * 1.1
        )
        is_angle_ok = (
            np.abs(measured_ctrl[1] - self.demandedAngle)
            <= self.min_angle_step * 1.1
        )
        return is_pos_ok and is_angle_ok

    def _is_demanded_as_ctrl(self, measured_ctrl: np.ndarray) -> bool:
        is_pos_ok = (
            np.abs(measured_ctrl[0] - self.demanded_pos_lvdt)
            <= self.min_position_step * 2
        )
        is_angle_ok = (
            np.abs(measured_ctrl[1] - self.demanded_angle_lvdt)
            <= self.min_angle_step * 2
        )
        return is_pos_ok and is_angle_ok

    def _is_demanded_as_lvdt(self, measured_lvdt: np.ndarray) -> bool:
        is_pos_ok = (
            np.abs(measured_lvdt[0] - self.demanded_pos_lvdt)
            <= self.min_position_step * 1.1
        )
        is_angle_ok = (
            np.abs(measured_lvdt[1] - self.demanded_angle_lvdt)
            <= self.min_angle_step * 1.1
        )
        return is_pos_ok and is_angle_ok

    def checkReadiness(self) -> None:
        self.get_all_crystal_para()
        if self.type == "position":
            if np.abs(self.crystalPositionCTRL - self.demandedPos) <= 50e-3:
                self.isCrystalReady = True
            else:
                self.isCrystalReady = False
        elif self.type == "angle":
            if np.abs(self.crystalAngleCTRL - self.demandedAngle) <= 3.0:
                self.isCrystalReady = True
            else:
                self.isCrystalReady = False

    def setParameter(self, value_to_set: t.Sequence[float], **kwargs):  # type: ignore
        if self.are_limits_ok(
            value_to_set[0], self.pos_limits
        ) and self.are_limits_ok(value_to_set[1], self.angle_limits):
            self._set_both(*value_to_set)
        else:
            logging.warning("Set not done, out of limits!")

    def are_limits_ok(
        self, set_value: float, limits: t.Sequence[float]
    ) -> bool:
        return limits[0] <= set_value <= limits[1]

    def getParameter(self, ele=None, getHeader=True, **kwargs):  # type: ignore
        ele_name = self.all_vars if ele is None else ele
        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        if "getHeader" in kwargs:
            getHeader = kwargs["getHeader"]
            del kwargs["getHeader"]

        if "only_ctrl" in kwargs:
            only_ctrl = kwargs.get("only_ctrl", False)
            del kwargs["only_ctrl"]
        else:
            only_ctrl = False

        ele_name = ele_name[-2:] if only_ctrl else ele_name

        if self.japc is not None:
            data = (
                self.japc.getParam(ele_name, getHeader, **kwargs)
                if getHeader
                else (
                    self.japc.getParam(ele_name, getHeader, **kwargs),
                    {},
                )
            )
            return data
        else:
            raise ValueError("No JAPC instance provided")

    def is_step_lost(self) -> np.ndarray:
        if self.japc is None:
            raise ValueError("No JAPC instance provided")
        step_loss = self.japc.getParam(
            [self.dw_lost_step_var, self.up_lost_step_var],
            timingSelectorOverride="",
        )
        return np.array(step_loss).squeeze()

    def _update_demanded_lvdt_values(
        self, position: float, angle: float
    ) -> None:
        self.demanded_pos_lvdt = position
        self.demanded_angle_lvdt = angle
        self.demanded_lvdt_values: npt.NDArray[np.float64] = np.array(
            [position, angle]
        )

    def _fill_hystory_buffer(
        self, lvdt_data: t.Sequence[float], ctrl_data: t.Sequence[float]
    ) -> None:
        self.teca_hystory["time"].append(datetime.datetime.now())
        self.teca_hystory["set_position"].append(self.demanded_pos_lvdt)
        self.teca_hystory["set_angle"].append(self.demanded_angle_lvdt)
        self.teca_hystory["measured_position"].append(lvdt_data[0])
        self.teca_hystory["measured_angle"].append(lvdt_data[1])
        self.teca_hystory["measured_position_ctrl"].append(ctrl_data[0])
        self.teca_hystory["measured_angle_ctrl"].append(ctrl_data[1])

    def _get_live_data(self) -> t.Tuple[np.ndarray, np.ndarray]:
        property_data, header = self.stream.wait_for_next()
        lvdt_data = np.array(
            [
                property_data[self.position_lvdt_field],
                property_data[self.angle_lvdt_field],
            ]
        )
        ctrl_data = np.array(
            [
                property_data[self.position_ctrl_field],
                property_data[self.angle_ctrl_field],
            ]
        )
        self._fill_hystory_buffer(lvdt_data, ctrl_data)
        return lvdt_data, ctrl_data

    def _fix_proposed_action(
        self,
        raw_correction: t.Sequence[float],
        measured_ctrl: t.Sequence[float],
    ) -> np.ndarray:
        logger.info(f"Proposed raw corection: {raw_correction}")

        correction_sign = np.sign(raw_correction)
        correction_module = np.abs(raw_correction)
        # proposed_correction_module = np.clip(
        #     correction_module, self.min_step_allowed, self.max_step_allowed
        # )
        proposed_correction_module = np.where(
            correction_module < self.min_step_allowed,
            0,
            np.abs(raw_correction),
        )

        logger.info(
            f"Fixed correction: {correction_sign * proposed_correction_module}"
        )

        return measured_ctrl + (correction_sign * proposed_correction_module)

    def set_lvdt_values(
        self,
        values_to_set: t.Sequence[float],
        pid_gains: t.Dict[str, npt.NDArray[np.float64]],
        time_out: float = 60,
        **kwargs: t.Any,
    ) -> None:
        counter: int = 0

        self._update_demanded_lvdt_values(*values_to_set)
        logger.info(f"Target values: {self.demanded_lvdt_values}")
        measured_lvdt, measured_ctrl = self._get_live_data()
        logger.info(
            f"Measured lvdt: {measured_lvdt}, Measured CTRL: {measured_ctrl}"
        )

        is_teca_at_lvdt = self._is_demanded_as_lvdt(measured_lvdt)
        if is_teca_at_lvdt:
            logger.info("teca at LVDT already")
            return

        initial_error = pid_gains["kp"] * (
            self.demanded_lvdt_values - measured_lvdt
        )
        logger.info(f"Initial error: {initial_error}")

        proposed_action = self._fix_proposed_action(
            initial_error, measured_ctrl
        )
        logger.info(f"New values (with clipping): {proposed_action}")

        self.setParameter(proposed_action, **kwargs)
        measured_lvdt, measured_ctrl = self._get_live_data()

        pid = PID(**pid_gains)

        is_teca_ready = self._is_set_as_ctrl(measured_ctrl)

        while not is_teca_at_lvdt:
            counter += 1
            if counter > time_out:
                logger.warning(
                    "Timed out...not exactly where we wanted to, moving on"
                )
                break
            is_teca_ready = False
            counter_ctrl = 0
            while not is_teca_ready:
                measured_lvdt, measured_ctrl = self._get_live_data()
                is_teca_ready = self._is_set_as_ctrl(measured_ctrl)
                counter_ctrl += 1
                if counter_ctrl > time_out:
                    logger.warning("Timed out...teca not moving correctly")
                    raise (ValueError("TECA not moving correctly"))

            measured_lvdt, measured_ctrl = self._get_live_data()

            is_teca_at_lvdt = self._is_demanded_as_lvdt(measured_lvdt)
            if is_teca_at_lvdt:
                break

            logger.info(
                f"Measured lvdt: {measured_lvdt}, Measured CTRL: {measured_ctrl}"
            )
            raw_correction, error = pid.propose_action_pid(
                self.demanded_lvdt_values, measured_lvdt
            )
            actual_action = self._fix_proposed_action(
                raw_correction, measured_ctrl
            )
            logger.info(f"New values (with clipping): {actual_action}")

            self.setParameter(actual_action, **kwargs)
        logger.info("TECA in position")


class PID:
    def __init__(
        self,
        ki: t.Sequence[float],
        kp: t.Sequence[float],
        kd: t.Sequence[float],
    ) -> None:
        self.integral_buffer: t.List[npt.NDArray[np.float64]] = [
            np.zeros_like(ki)
        ]
        self.error_buffer: t.List[npt.NDArray[np.float64]] = [
            np.zeros_like(ki)
        ]
        self.pid_gains: t.Dict[str, npt.NDArray[np.float64]] = dict(
            ki=ki, kp=kp, kd=kd
        )

    def propose_action_pid(
        self,
        target_lvdt: npt.NDArray[np.float64],
        measured_lvdt: npt.NDArray[np.float64],
    ) -> t.Tuple[npt.NDArray[np.float64], npt.NDArray[np.float64]]:
        error: npt.NDArray[np.float64] = target_lvdt - measured_lvdt

        proportional: npt.NDArray[np.float64] = self.pid_gains["kp"] * error
        logger.info(f"Proportional: {proportional}")

        integral: npt.NDArray[np.float64] = (
            self.integral_buffer[-1] + self.pid_gains["ki"] * error
        )
        logger.info(f"Integral: {integral}")

        derivative: npt.ArrayLike = self.pid_gains["kd"] * (
            error - self.error_buffer[-1]
        )
        logger.info(f"Derivative: {derivative}")

        proposed_correction: npt.ArrayLike = (
            proportional + derivative + integral
        )
        self.integral_buffer.append(integral)
        self.error_buffer.append(error)
        return proposed_correction, error
