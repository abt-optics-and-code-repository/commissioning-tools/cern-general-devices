import numpy as np
import matplotlib.pyplot as plt
import datetime
from scipy.optimize import curve_fit
from cern_general_devices import Device
import pickle
import logging
from pathlib import Path


class BGIPXL(Device):
    def __init__(self, japc, name, timingSelectorOverride=None, plane=None, N_turns=30):
        """Class to handle BGIPXL FESA class v0.31.0 of the PS BGI:
                (i) Available device names: PR.BGI82 and PR.BGI84

                For more info see:
                    - https://wikis.cern.ch/display/BEBI/BGIPXL+user+manual
                    - https://ccde.cern.ch/devices/classes/21904/version/38901/properties

                Args:
                        japc (PyJapc): Instance of PyJapc object
                        name (str): device name
                        timingSelectorOverride (str, optional): Timing selector override, for example: SPS.USER.SFTPRO2. Defaults to None.
                """
        super().__init__(japc, name, timingSelectorOverride)

        if "PR.BGI" in self.name:
            logging.debug('BGIPXL class constructor')
        else:
            logging.warning('Careful: this class is written for BGI devices employing BGIPXL FESA class!')

        # Plane to acquire (set by default as None, make checks that correct device is bring used)
        self.plane = plane
        # Number of columns in TbT plots
        self.Ncols = 5
        # Number of turns to be analysed
        self.N_turns = N_turns
        # Fit error check
        self.fit_error_check = 0.25

    def getParameter(self, property='ProfileAcquisition', getHeader=True, **kwrds):
        data_raw = self.japc.getParam(self.name + '/' + property,
                                      getHeader=getHeader,
                                      timingSelectorOverride=self.cycle_selector_override,
                                      **kwrds)

        self.acq, self.acq_info = data_raw if getHeader else (data_raw, {})

        # Handle enums
        if property == 'MeasurementSetting':
            self.acq['mode'] = self.acq['mode'][0]
            self.acq['profileMeasurementTimingUnit'] = self.acq['profileMeasurementTimingUnit'][0]
            self.acq['tvMeasurementTimingUnit'] = self.acq['tvMeasurementTimingUnit'][0]

        return self.acq, self.acq_info

    def getTbTData(self, **kwds):
        self.getParameter(**kwds)

        # Get position and profile data
        self.acq['pos'] = self.acq['profileTransversePositions_um'].astype(np.float) / 1e3
        self.acq['data'] = self.acq['profiles'][:self.N_turns].astype(np.float)

        return self.acq, self.acq_info


    def setupTbTmeasurement(self, profileMeasurementCount=50, profileMeasurementStart=100, profileMeasurementOffset_25ns=0):
        """Sets up the BGI instrument for turn-by-turn (TbT) beam profile measurements

        Args:
            profileMeasurementCount (int, optional): Number of turns/profiles to measure
            profileMeasurementStart (int, optional): Which turn to start measuring on
            profileMeasurementOffset_25ns (int, optional): Offset of the start of each measurement in steps of 25 ns
        """
        mode = 1 # (enum) 1 = BEAM_PROFILES
        profileMeasurementInterval = 1 # One profile every turn
        profileMeasurementIntegrationTime = 1 # Integrate over one turn
        profileMeasurementTimingUnit = 3 # (enum) 3 = RF
        rawDataSaveLocation = '' # Not used in BEAM_PROFILES mode
        self.setupMeasurement(mode, profileMeasurementInterval, profileMeasurementCount,
                              profileMeasurementIntegrationTime, profileMeasurementTimingUnit,
                              profileMeasurementStart, profileMeasurementOffset_25ns, rawDataSaveLocation)


    def setupMeasurement(self,
                         mode=1, # (enum) 1 = BEAM_PROFILES
                         profileMeasurementInterval=1*10000, # 10000*100ns = 1 ms
                         profileMeasurementCount=10, # 10 profiles
                         profileMeasurementIntegrationTime=1*10000, # 10000*100ns = 1 ms
                         profileMeasurementTimingUnit=2, # (enum) 2 = NS_100
                         profileMeasurementStart=170*10000, # C-time 170 ms
                         profileMeasurementOffset_25ns=0,
                         rawDataSaveLocation=''):
        """Sets up a the BGI instrument for beam profile measurements.
            Default argument values results in 10 profiles integrated over 1 ms starting from C-time 170 ms.

        Args:
            mode (int, optional): Which mode to use, [0 = OFF, 1 = BEAM_PROFILES, 2 = RAW_DATA, 4 = TV]
            profileMeasurementInterval (int, optional): The interval between two profile measurements, in units set by 'profileMeasurementTimingUnit'
            profileMeasurementCount (int, optional): Number of profiles to measure
            profileMeasurementIntegrationTime (int, optional): The integration time of one profile, in units set by 'profileMeasurementTimingUnit'
            profileMeasurementTimingUnit (int, optional): Time unit to use, [0 = MS_1, 1 = NS_100, 2 = NS_25, 3 = RF turns]
            profileMeasurementStart (int, optional): Which turn to start measuring on
            profileMeasurementOffset_25ns (int, optional): Offset of the start of each measurement in steps of 25 ns           
        """
        self.settings, _ = self.getParameter(property='MeasurementSetting')
        self.previous_settings = self.settings.copy()

        # Acquisition mode
        self.settings['mode'] = mode
        # Number of turns between profile acquisitions
        self.settings['profileMeasurementInterval'] = profileMeasurementInterval
        # Number of profile acquisitions
        self.settings['profileMeasurementCount'] = profileMeasurementCount
        # Integration length per profile
        self.settings['profileMeasurementIntegrationTime'] = profileMeasurementIntegrationTime
        # Time unit
        self.settings['profileMeasurementTimingUnit'] = profileMeasurementTimingUnit
        # Delay before starting integration
        self.settings['profileMeasurementStart'] = profileMeasurementStart
        # Fine delay before starting integration
        self.settings['profileMeasurementOffset_25ns'] = profileMeasurementOffset_25ns
        # Where to dump saved data (Used when mode is 2 = 'RAW_DATA')
        self.settings['rawDataSaveLocation'] = rawDataSaveLocation

        self.japc.setParam(self.name + '/MeasurementSetting', self.settings,
                           timingSelectorOverride=self.cycle_selector_override)

        logging.info('Settings sent to ' + str(self.name) + ': ' + str(self.settings))


    def restorePreviousMeasurementSettings(self):
        """Restores the measurement settings before the last call to setupMeasurement(), if available"""
        if hasattr(self, 'previous_settings') and self.previous_settings is not None:
            logging.info(f'Restoring previous settings to {str(self.name)}: {str(self.previous_settings)}')
            self.japc.setParam(self.name + '/MeasurementSetting', self.previous_settings,
                               timingSelectorOverride=self.cycle_selector_override)

    def _gaussian(self, x, a, I, mu, sig):
        if sig == 0:
            return np.zeros(len(x))
        else:
            return a + I / np.sqrt(2 * np.pi * sig ** 2) * np.exp(-(x - mu) ** 2 / 2. / sig ** 2)

    def _gauss_fit(self, x, y):
        try:
            mu = np.average(x, weights=np.abs(y))
            sigma = np.sqrt(np.average(x ** 2, weights=np.abs(y)) - mu ** 2)
            p0 = [y.min(), (np.max(y) - np.min(y)) * np.sqrt(2 * np.pi * sigma ** 2), mu, sigma]
            popt, pcov = curve_fit(self._gaussian, x, y, p0=p0)
        except (RuntimeError, ZeroDivisionError):
            popt, pcov = np.zeros(4), np.ones((4, 4)) * 1e12
            logging.warning('profile fit failure on ' + self.name)

        fit_error = np.sqrt(np.diagonal(pcov))

        # Force sigma to be returned positive
        popt[3] = np.abs(popt[3])

        return popt, pcov, fit_error

    def TbTfitting(self):
        self.fit_popt = []
        self.fit_pcov = []
        self.fit_error = []

        for i in self.acq['data']:
            popt, pcov, error = self._gauss_fit(self.acq['pos'], i)
            self.fit_popt.append(popt)
            self.fit_pcov.append(pcov)
            self.fit_error.append(error)

        self.fit_popt = np.array(self.fit_popt)
        self.fit_pcov = np.array(self.fit_pcov)
        self.fit_error = np.array(self.fit_error)

        return self.fit_popt, self.fit_pcov, self.fit_error

    def plotNrows(self):
        if (self.N_turns / self.Ncols - np.floor(self.N_turns / self.Ncols)) == 0:
            return int(np.floor(self.N_turns / self.Ncols))
        else:
            return int(np.floor(self.N_turns / self.Ncols) + 1)

    def plotTbTfit(self, noFit=False, saveFig=''):
        figName = 'TbT Fitting for ' + self.name + '(' + self.getPlane() + ')'

        if not plt.fignum_exists(figName): plt.subplots(ncols=self.Ncols,
                                                        nrows=self.plotNrows(),
                                                        figsize=(9, 10),
                                                        num=figName)

        ax = np.array(plt.figure(figName).axes)
        ax = np.reshape(ax, (self.plotNrows(), self.Ncols))

        for idx, row in enumerate(ax):
            for jdx, col in enumerate(row):
                col.clear()
                col.plot(self.acq['pos'], self.acq['data'][idx * self.Ncols + jdx], 'x')
                xrange = np.arange(np.min(self.acq['pos']), np.max(self.acq['pos']),
                                   (np.max(self.acq['pos']) - np.min(self.acq['pos'])) / 100.)

                if noFit:
                    col.set_title('Turn ' + str(idx * self.Ncols + jdx))
                else:
                    if self.fit_error[idx * self.Ncols + jdx, 3] > self.fit_error_check * self.fit_popt[idx * self.Ncols + jdx, 3]:
                        col.set_title('Fit error!:\n Turn ' + str(idx * self.Ncols + jdx))
                        col.plot(xrange, self._gaussian(xrange, self.fit_popt[idx * self.Ncols + jdx, 0],
                                                        self.fit_popt[idx * self.Ncols + jdx, 1],
                                                        self.fit_popt[idx * self.Ncols + jdx, 2],
                                                        self.fit_popt[idx * self.Ncols + jdx, 3]), 'r')
                    else:
                        col.plot(xrange, self._gaussian(xrange, self.fit_popt[idx * self.Ncols + jdx, 0],
                                                        self.fit_popt[idx * self.Ncols + jdx, 1],
                                                        self.fit_popt[idx * self.Ncols + jdx, 2],
                                                        self.fit_popt[idx * self.Ncols + jdx, 3]))
                        col.set_title('Turn ' + str(idx * self.Ncols + jdx) + ':\n' + r'$\sigma = $' + str(
                            np.round(self.fit_popt[idx * self.Ncols + jdx, 3], 1)) + r'$\pm$' + str(
                            np.round(self.fit_error[idx * self.Ncols + jdx, 3], 1)) + ' mm')

                    col.set_xlabel(self.getPlane() + ' [mm]')

        plt.suptitle(self.name + ' ' + str(self.acq_info['cycleStamp']))
        plt.figure(figName).tight_layout()
        plt.pause(0.01)

        if saveFig:
            plt.savefig(saveFig + '/' + self.name + '_pltTbTfit_' + str(self.acq_info['cycleStamp']) + '.png', dpi=600)

    def plotTbT(self,saveFig=''):
        figName = 'TbT Analysis for ' + self.name + ' (' + self.getPlane() + ')'

        if not plt.fignum_exists(figName): plt.subplots(ncols=1, nrows=2, figsize=(5, 6), num=figName)

        ax = plt.figure(figName).axes

        ax[0].clear()
        ax[1].clear()

        ax[0].plot(np.arange(len(self.fit_popt[:, 3])), self.fit_popt[:, 3])

        ax[0].set_xlabel('Turn number')
        ax[0].set_ylabel(r'$\sigma$ (' + self.getPlane() + ') [mm]')
        ax[0].set_title(self.acq_info['cycleStamp'])

        if any(self.fit_error[:, 3] > self.fit_error_check * self.fit_popt[:, 3]):
            idx_error = np.where(self.fit_error[:, 3] > self.fit_error_check * self.fit_popt[:, 3])
            ax[0].plot(np.arange(len(self.fit_popt[:, 3]))[idx_error], self.fit_popt[:, 3][idx_error], 'xr')

        ax[1].plot(np.arange(len(self.fit_popt[:, 2])), self.fit_popt[:, 2])
        ax[1].set_xlabel('Turn number')
        ax[1].set_ylabel(r'$\mu$ (' + self.getPlane() + ') [mm]')

        if any(self.fit_error[:, 3] > self.fit_error_check*self.fit_popt[:, 3]):
            idx_error = np.where(self.fit_error[:, 3] > self.fit_error_check*self.fit_popt[:, 3])
            ax[1].plot(np.arange(len(self.fit_popt[:, 2]))[idx_error], self.fit_popt[:, 2][idx_error],'xr')

        plt.figure(figName).tight_layout()
        plt.pause(0.01)

        if saveFig:
            plt.savefig(saveFig + '/' + self.name + '_pltTbT_' + str(self.acq_info['cycleStamp']) + '.png', dpi=600)

    def plotTbTintensity(self,saveFig=''):
        figName = 'TbT Intensity for ' + self.name + ' (' + self.getPlane() + ')'

        if not plt.fignum_exists(figName): plt.subplots(ncols=1, nrows=2, figsize=(5, 6), num=figName)

        ax = plt.figure(figName).axes

        y = self.getTbTintensity(flag='fromData')

        ax[0].clear()
        ax[1].clear()

        ax[0].plot(np.arange(len(y)), y)
        ax[0].set_xlabel('Turn number')
        ax[0].set_ylabel(r'Intensity [arb.]')
        ax[0].set_title(self.acq_info['cycleStamp'])

        if any(self.fit_error[:, 3] > self.fit_error_check*self.fit_popt[:, 3]):
            idx_error = np.where(self.fit_error[:, 3] > self.fit_error_check*self.fit_popt[:, 3])
            ax[0].plot(np.arange(len(y))[idx_error], y[idx_error],'xr')

        if y[0] > 0:
            ax[1].plot(np.arange(len(y)), y / y[0])
            ax[1].set_xlabel('Turn number')
            ax[1].set_ylabel(r'Relative Intensity [vs. turn 0]')

            if any(self.fit_error[:, 3] > self.fit_error_check*self.fit_popt[:, 3]):
                idx_error = np.where(self.fit_error[:, 3] > self.fit_error_check*self.fit_popt[:, 3])
                ax[1].plot(np.arange(len(y))[idx_error], y[idx_error] / y[0],'xr')
        else:
            logging.warning('No beam intenisty (or fit failed) on first turn')

        plt.figure(figName).tight_layout()
        plt.pause(0.01)

        if saveFig:
            plt.savefig(saveFig + '/' + self.name + '_pltTbTintenisty_' + str(self.acq_info['cycleStamp']) + '.png', dpi=600)

    def getPlane(self):
        if 'BGI' and '82' in self.name:
            return "H"
        elif 'BGI' and '84' in self.name:
            return "V"
        else:
            return "Plane undefined!"

    def getTbTintensity(self, flag='fromFit'):
        if flag == 'fromFit':
            return self.fit_popt[:, 1]
        elif flag == 'fromData':
            return np.sum(self.acq['data'], axis=1)

    def checkPlane(self):
        if self.plane == None:
            logging.warning('No check made of plane requested (H or V) on ' + self.name)
        elif '82' in self.name:
            if not self.plane == 'H':
                logging.warning('Wrong plane acquired for ' + self.name + ', plane is H')
        elif '84' in self.name:
            if not self.plane == 'V':
                logging.warning('Wrong plane acquired for ' + self.name + ', plane is V')

