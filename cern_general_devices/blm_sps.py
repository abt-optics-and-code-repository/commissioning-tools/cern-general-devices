import numpy as np
import pandas as pd
from cern_general_devices import Device
import logging
import typing as t
import pyjapc


class BlmSPS(Device):
    """BlmSPS."""

    def __init__(
        self,
        japc: pyjapc.PyJapc,
        lss: t.Union[t.List[str], str] = "all",
        ba: t.Union[t.List[str], str] = "all",
        timingSelectorOverride: t.Optional[str] = None,
    ) -> None:
        """__init__.

        Class to handle SPS BLMs all around the ring.
        Parameters
        ----------
        japc :
            japc
        lss : List[str]
            List of LSS number to use
        ba : List[str]
            List of BAs number to use
        timingSelectorOverride :
            timingSelectorOverride
        """

        super().__init__(japc, "blm-sps", timingSelectorOverride)

        self.japc = japc
        if self.japc is not None:
            self.selector = (
                timingSelectorOverride
                if timingSelectorOverride is not None
                else self.japc.getSelector()
            )
        self.beam_loss_field = "/Acquisition#beamLosses_gray"
        self.channel_names_field = "/Acquisition#channelNames"
        if lss == "all":
            self.blm_lss_vars = [
                f"BLRSPS_LSS{lss_value}" for lss_value in range(1, 7)
            ]
        elif lss is None:
            self.blm_lss_vars = []
        else:
            self.blm_lss_vars = [
                f"BLRSPS_LSS{lss_value}" for lss_value in lss
            ]

        if ba == "all":
            self.blm_ba_var = [
                f"BLRSPS_BA{ba_value}" for ba_value in range(1, 7)
            ]
        elif ba is None:
            self.blm_ba_var = []
        else:
            self.blm_ba_var = [f"BLRSPS_BA{ba_value}" for ba_value in ba]

        if "BLRSPS_LSS3" in self.blm_lss_vars:
            del self.blm_lss_vars[self.blm_lss_vars.index("BLRSPS_LSS3")]

        self.ba_lss = self.blm_ba_var + self.blm_lss_vars
        self.channel_names_var = [
            blm_var + self.channel_names_field for blm_var in self.ba_lss
        ]
        self.losses_vars = [
            blm_var + self.beam_loss_field for blm_var in self.ba_lss
        ]

        if self.ba_lss != []:
            channel_names = self.japc.getParam(
                self.channel_names_var,
                timingSelectorOverride=self.selector,
            )
            self.channel_names = np.array(channel_names).flatten()
            self.select_useful()
        else:
            print("No BA nor LSS declared.")

    def getParameter(
        self,
        ele: t.Optional[str] = None,
        getHeader: bool = True,
        **kwargs: t.Dict[t.Any, t.Any],
    ) -> t.Tuple[t.Any, t.Any]:
        """getParameter.

        Parameters
        ----------
        getHeader :
            Boolean
        kwargs :
            kwargs
        """
        info: t.Dict[str, t.Any] = {}

        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )

        if "getHeader" in kwargs:
            del kwargs["getHeader"]

        losses, info = self._get_all_blm()
        losses = losses[self.mask_blm]
        data = {key: value for key, value in zip(self.useful_blms, losses)}

        return (data, info)

    def analyse_data(self, losses_raw_data: t.List[t.List[float]]) -> t.Dict:
        losses = np.array(losses_raw_data).flatten()
        losses = losses[self.mask_blm]
        data = {key: value for key, value in zip(self.useful_blms, losses)}
        return data

    def _get_all_blm(self) -> t.Tuple[np.ndarray, t.Any]:
        """_get_all_blm."""

        if self.japc is not None:
            losses_vec, info = self.japc.getParam(
                self.losses_vars,
                getHeader=True,
                timingSelectorOverride=self.selector,
            )
            losses_vec = np.array(losses_vec).flatten()

        return losses_vec, info

    def get_value(self) -> t.Tuple[np.ndarray, t.Any]:
        """get_value."""
        losses_vec, info = self._get_all_blm()
        self.losses_vec = losses_vec
        self.timeStamp = info["acqStamp"]
        return self.losses_vec, self.timeStamp

    def select_useful(self) -> None:
        """select_useful."""
        self.mask_blm = (
            (self.channel_names != "Spare")
            & (self.channel_names != "")
            & (self.channel_names != "SPARE")
        )
        self.useful_blms = self.channel_names[self.mask_blm]
