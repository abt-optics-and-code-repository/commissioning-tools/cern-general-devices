import logging
import typing as t
import abc
import pyjapc


class Device(abc.ABC):
    def __init__(
        self,
        # japc: t.Optional[pyjapc.PyJapc],
        japc: pyjapc.PyJapc,
        name: str,
        timingSelectorOverride: str | None = None,
    ) -> None:
        """Generic Japc device interface

        Args:
            japc (t.Optional[pyjapc.PyJapc]): Instance of PyJapc
            object from main
            name (str): Name of the device
            timingSelectorOverride (str, optional): String to override
            the timing selector. Defaults to None.
        """
        self.japc = japc
        if timingSelectorOverride is not None:
            self.cycle_selector_override = timingSelectorOverride
        else:
            if self.japc is not None:
                self.cycle_selector_override = self.japc.getSelector()
            else:
                self.cycle_selector_override = ""
        logging.debug(self.cycle_selector_override)
        self.name = name

    def print_name(self) -> None:
        """Function to print name"""
        print(self.name)

    def __str__(self) -> str:
        return self.name

    def setParameter(self, **kwargs: t.Dict) -> None:
        """Wrapper around the setParam of PyJapc"""
        pass

    @abc.abstractmethod
    def getParameter(
        self,
        ele: str | None = None,
        getHeader: bool = True,
        **kwargs: t.Dict[t.Any, t.Any]
    ) -> t.Tuple[t.Any, dict]:
        """Wrapper around the getParam method of PyJapc

        Args:
            ele (str, optional): String representing the device
            field (e.g. device/property#field). If None, then
            default action is performed. Defaults to None.
            getHeader (bool, optional): Boolean to get header
            from device field. Defaults to True.

        Returns:
            typing.Tuple[any, dict]: Tuple like (data, info).
        """
        pass
