import numpy as np
import matplotlib.pyplot as plt
import pyjapc
from cern_general_devices import Device
import time

class IPOC(Device):
    def __init__(self, japc, timingSelectorOverride=None):
        super().__init__(japc, "ipoc", timingSelectorOverride)

        self.japc = japc
        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )

        self.mkdh_wf_vars = [
            "MKDH.ECA5.IPOC.CTS.1A/Waveform#waveformData",
            "MKDH.ECA5.IPOC.CTS.1B/Waveform#waveformData",
            "MKDH.ECA5.IPOC.CTS.2A/Waveform#waveformData",
            "MKDH.ECA5.IPOC.CTS.2B/Waveform#waveformData",
            "MKDH.ECA5.IPOC.CTS.3A/Waveform#waveformData",
            "MKDH.ECA5.IPOC.CTS.3B/Waveform#waveformData",
        ]
        self.mkdv_wf_vars = [
            "MKDV.ECA5.IPOC.CTS.1A/FilteredWaveform#waveformData",
            "MKDV.ECA5.IPOC.CTS.1B/FilteredWaveform#waveformData",
            "MKDV.ECA5.IPOC.CTS.2A/FilteredWaveform#waveformData",
            "MKDV.ECA5.IPOC.CTS.2B/FilteredWaveform#waveformData",
            "MKDV.ECA5.IPOC.CTS.3A/FilteredWaveform#waveformData",
            "MKDV.ECA5.IPOC.CTS.3B/FilteredWaveform#waveformData",
        ]

        self.start_time_mkdv_var = (
            "MKDV.ECA5.IPOC.CTS.1A/FilteredWaveform#startTime"
        )
        self.start_time_mkdh_var = (
            "MKDH.ECA5.IPOC.CTS.1A/Waveform#startTime"
        )
        self.sampling_mkdv_var = (
            "MKDV.ECA5.IPOC.CTS.1A/FilteredWaveform#samplePeriod"
        )
        self.sampling_mkdh_var = (
            "MKDH.ECA5.IPOC.CTS.1A/Waveform#samplePeriod"
        )

        self.energy_var = "MKD.ECA5.MKCB/PostOperation#energy"

        self.mkdh_volt_var = [
            "MKD.ECA5.MKCBI.MKDH12/PostOperation#signalCh1",
            "MKD.ECA5.MKCBI.MKDH3/PostOperation#signalCh1",
        ]

        self.mkdv_volt_var = [
            "MKD.ECA5.MKCBI.MKDV12/PostOperation#signalCh1",
            "MKD.ECA5.MKCBI.MKDV3/PostOperation#signalCh1",
        ]

        self.all_vars = [
            self.energy_var,
            *self.mkdh_volt_var,
            *self.mkdv_volt_var,
            *self.mkdh_wf_vars,
            *self.mkdv_wf_vars,
        ]
        
        self.mkdv_vars = [
                *self.mkdv_volt_var, 
                *self.mkdv_wf_vars,
                self.start_time_mkdv_var,
                self.sampling_mkdv_var,
            ]


    def getParameter(self, ele_name=None, getHeader=True, **kwargs):
        info = {}
        ele_name = self.all_vars if ele_name is None else ele_name
        kwargs["timingSelectorOverride"] = kwargs.get("timingSelectorOverride", self.selector)
        data = (
            self.japc.getParam(
                ele_name,
                getHeader,
                **kwargs
            )
            if getHeader
            else (
                self.japc.getParam(
                    ele_name,
                    getHeader,
                    **kwargs
                ),
                info,
            )
        )
        return data

    def get_mkdv_waveform(self, **kwargs):
        kwargs["timingSelectorOverride"] = kwargs.get("timingSelectorOverride", self.selector)
        data = self.japc.getParam(
                self.mkdv_vars,
                **kwargs
            )
        v1, v2, wf1a, wf1b, wf2a, wf2b, wf3a, wf3b, start_t, sampling = data
        time = np.arange(start_t, start_t + sampling * (len(wf1a)), sampling)

        return data, time


# from cern_general_devices import sps

# acc = sps.SPS()

# def callbak(param, new_value, header):
#     print("cycle:")
#     print(header["selector"])
#     print("---------------")
#     time.sleep(0.5)
#     data = ipoc.getParameter(timingSelectorOverride=header["selector"])
#     data_mkdv, time = ipoc.get_mkdv_waveform(timingSelectorOverride=header["selector"])
#     print(data)
    
# japc.subscribeParam(acc.beam_out, callbak, getHeader=True)

# japc.startSubscriptions()    

# japc.stopSubscriptions()
# japc.clearSubscriptions()