import numpy as np
import datetime
from cern_general_devices import Device
import logging


class MKPdelays(Device):
    def __init__(self, japc, limits, mkp_index=None, typeScan=None, timingSelectorOverride=None):

        super().__init__(japc, "MKPdelays", timingSelectorOverride)

        self.individual_mkp_selector = ""
        self.japc = japc
        self.limits = limits
        self.mkp_index = mkp_index
        self.type = typeScan

        self.mkpStting = "MKPfn/ExpertSettingDevice"
        self.mkpAcquisition = "MKP.BA1.F3.PFN."

        self.all_vars = []
        for i in range(8):
            self.all_vars.append("{}{}/ExpertSettingDevice".format(self.mkpAcquisition, i+1))
        self.all_vars.append("MKP.BA1.F3.CONTROLLER/Setting#arrKickDelay")
        
        #IF MKP INDEX IS USED, DROP VARIABLES OF MKPS NOT INVOLVED 
        if self.mkp_index is not None:
            self.all_vars = [self.all_vars[j] for j in self.mkp_index]
            print("\nIN MKP DEVICE: only MKPs with index {}: \n".format(self.mkp_index))
        
        self.dof = len(self.all_vars)
        self.setup_template() #to set up template for setting parameters: first value in array important for MKPs

    # Method to set up template to fill in for setting parameter, taken from device "kicker.py"
    def setup_template(self):
        self.field_template = self.getParameter(
            getHeader=False
            # original=True,
        )
        print("template: ", self.field_template)

    # Method to extract all data at once
    def get_all_mkp_para(self):
        all_data = self.japc.getParam(self.all_vars)
        return all_data

    # Methods to check that values fall within limits
    def are_limits_ok(self, set_value, limits):
        return limits[0] <= set_value <= limits[1]

    # Method to set specific individual mkp time delays, or set
    def setParameter(self, mkp_nr, value, **kwargs):
        if mkp_nr <= 8:
            if self.are_limits_ok(value, self.MKP_ind_limits):
                self.field_template[0][mkp_nr-1]["arrG2MainSwitchFineTimingDelay"] = int(value)
                self.japc.setParam(self.all_vars[mkp_nr-1], self.field_template[0][mkp_nr-1][0], timingSelectorOverride="", checkDims=False)
                print("Setting individual MKP switch {} time shift to {:.2f} ns\n".format(mkp_nr, value))
            else:
                logging.warning(
                    "Set not done for switch nr {}, out of limits!".format(
                        mkp_nr
                    )
                )
        elif mkp_nr == 9:
            if self.are_limits_ok(value, self.MKP_gen_limits):
                self.field_template[2][1] = int(value)
                self.japc.setParam(self.all_vars[mkp_nr-1], self.field_template[2], checkDims=False)
                print("Setting general MKP time shift to {:.2f} ns\n".format(value))
            else:
                logging.warning(
                    "Set not done for general time shift, out of limits!"
                )

    # Method to set all individual mkp time delays + general time shift
    def set_all_mkp_para(self, dtau):
        # Set individual time shifts
        for i in range(self.dof - 1):
            if self.are_limits_ok(dtau[i], self.limits[:,i]):
                self.field_template[0][i]["arrG2MainSwitchFineTimingDelay"][0] = int(dtau[i])
                self.japc.setParam(self.all_vars[i], self.field_template[0][i], timingSelectorOverride="", checkDims=False)
                print("Setting MKP switch {} time shift to {} ns\n".format(i+1, self.field_template[0][i]))
            else:
                logging.warning("Set not done for switch nr {}, out of limits!".format(i+1))

        #Set general time shifts if exists, or just the index corresponding to the last individual MKP time shift
        if self.mkp_index is None or 8 in self.mkp_index: 
            if self.are_limits_ok(dtau[-1], self.limits[:,-1]):
                self.field_template[2][1] = int(dtau[-1])
                self.japc.setParam(self.all_vars[-1], self.field_template[2], checkDims=False)
                print("Setting general MKP time shift to {} ns\n".format(self.field_template[2]))
            else:
                logging.warning(
                    "Set not done for MKP general time shift, out of limits!"
                )
        else:  #no general time shift, only individual shift 
            if self.are_limits_ok(dtau[-1], self.limits[:,-1]):
                self.field_template[0][-1]["arrG2MainSwitchFineTimingDelay"][0] = int(dtau[-1])
                self.japc.setParam(self.all_vars[-1], self.field_template[0][-1], timingSelectorOverride="", checkDims=False)
                print("Setting MKP switch {} time shift to {} ns\n".format(self.dof+1, self.field_template[0][-1]))
            else:
                logging.warning("Set not done for switch nr {}, out of limits!".format(self.dof+1))


    def getParameter(self, ele_name=None, getHeader=True, **kwargs):
        info = {}
        ele_name = self.all_vars if ele_name is None else ele_name
        kwargs["timingSelectorOverride"] = self.individual_mkp_selector
        if "getHeader" in kwargs:
            getHeader = kwargs["getHeader"]
            del kwargs["getHeader"]

        #Individual MKP shift
        data_individual = (
            self.japc.getParam(ele_name[:-1], getHeader, **kwargs)
            if getHeader
            else (
                    self.japc.getParam(ele_name[:-1], getHeader, **kwargs),
                info,
            )
        )

        #Add selector if general time shift is included, otherwise data_general will only represent the last MKP index 
        if self.mkp_index is None or 8 in self.mkp_index: 
            kwargs["timingSelectorOverride"] = self.japc.getSelector()

        data_general = (
                self.japc.getParam(ele_name[-1], getHeader, **kwargs)
                if getHeader
                else (
                        self.japc.getParam(ele_name[-1], getHeader, **kwargs),
                        info,
                )
            )

        #Concatenate tuple from individual and general time shift with info at the end,
        data = data_individual + data_general

        return data
