import numpy as np
import pandas as pd
from cern_general_devices import Device
import logging
import pyjapc
import typing as t

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)


class BLMI(Device):
    def __init__(
        self,
        japc: pyjapc.PyJapc,
        tl: str,
        timingSelectorOverride: str | None = None,
    ) -> None:
        super().__init__(japc, "BLMI", timingSelectorOverride)

        if japc is None:
            japc = pyjapc.PyJapc(noSet=True)
            LOG.warning("No JAPC instance provided, creating a new one")
        self.japc = japc
        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.tl = tl

        self.tl_number = 2 if "2" in self.tl else 8

        if self.tl_number == 2:
            self.blm_devices = [
                "BLMITI2UP",
                "BLMITI2DWN",
            ]
        else:
            self.blm_devices = [
                "BLMITT40",
                "BLMITI8DWN",
            ]

        self.blm_field = "/Acquisition#normLosses"
        self.blm_channels_field = "/Acquisition#channelNames"

        self.channel_names_variables = [
            device + self.blm_channels_field for device in self.blm_devices
        ]

        self.losses_var = [
            device + self.blm_field for device in self.blm_devices
        ]

        self.channel_names = self.japc.getParam(
            self.channel_names_variables, timingSelectorOverride=self.selector
        )
        self.channel_names = np.concatenate(self.channel_names)
        self.select_useful()

    def getParameter(
        self,
        ele: str | None = None,
        getHeader: bool = True,
        **kwargs: t.Dict[t.Any, t.Any]
    ) -> t.Tuple[t.Dict[str, np.ndarray], t.Dict[str, t.Any]]:
        info: t.Dict[str, t.Any] = {}

        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )

        if "getHeader" in kwargs:
            getHeader: kwargs["getHeader"]  # type: ignore
            del kwargs["getHeader"]

        losses, info = self._get_all_blm()
        data = {key: value for key, value in zip(self.useful_blms, losses)}

        return data, info

    def _get_raw_data(
        self,
    ) -> t.Tuple[t.List[np.ndarray], t.Dict[str, t.Any]]:
        if self.japc is not None:
            data_temp, info = self.japc.getParam(
                self.losses_var,
                getHeader=True,
                timingSelectorOverride=self.selector,
            )
            return data_temp, info
        else:
            raise ValueError("No JAPC instance provided")

    def _get_all_blm(self) -> t.Tuple[np.ndarray, t.Dict[str, t.Any]]:
        data_temp, info = self._get_raw_data()
        losses_masked = self.clean_data(data_temp)
        return losses_masked, info

    def clean_data(self, raw_data: t.List[np.ndarray]) -> np.ndarray:
        losses_vec = np.concatenate(raw_data)
        losses_masked = losses_vec[self.mask_blm]
        return losses_masked

    def get_value(self) -> t.Tuple[np.ndarray, int]:
        losses_vec, info = self._get_all_blm()
        self.losses_vec = losses_vec
        self.timeStamp = info["acqStamp"]
        return self.losses_vec, self.timeStamp

    def select_useful(self) -> None:
        self.useful_blms = list(
            filter(
                lambda x: x != "Spare"
                and "TT66" not in x
                and x != "SPARE"
                and x != "",
                self.channel_names,
            )
        )
        LOG.info(self.useful_blms)
        self.mask_blm = [
            ele in self.useful_blms for ele in self.channel_names
        ]

    def make_losses_df(self, losses: np.ndarray) -> pd.Series:
        df = pd.DataFrame(
            losses, index=self.useful_blms, columns=["lossesBLM"]
        )
        return df["lossesBLM"]

    def calculate_losses_norm(
        self, losses: t.List[np.ndarray], intExtr: float
    ) -> pd.Series:
        data_cleaned = self.clean_data(losses)
        tot_loss = self.make_losses_df(data_cleaned)
        return tot_loss / intExtr

    def get_normalised_losses(self, intensity: float) -> pd.Series:
        data_raw, info = self._get_raw_data()
        return self.calculate_losses_norm(data_raw, intensity)


if __name__ == "__main__":
    import pyjapc

    japc = pyjapc.PyJapc("SPS.USER.LHCPILOT")

    blm = BLMI(japc, tl="ti8")
    LOG.info(blm.getParameter())

    data_raw, info = blm._get_raw_data()
    LOG.info(blm.calculate_losses_norm(data_raw, 5e9))
