import numpy as np
from cern_general_devices import Device
import logging


class EarlyDump(Device):
    def __init__(self, japc, timingSelectorOverride=None):
        """
        Class to set and get the SPS early dump using the TSU classes.
        Args:
            japc (PyJapc): Instance of PyJapc object
            timingSelectorOverride (str, optional): Timing selector override, for example: SPS.USER.SFTPRO2. Defaults to None.
        """

        super().__init__(japc, "early_dump", timingSelectorOverride)

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.data = []
        self.delay_var_name = (
            "MKDTIM.ECA5.TSUA/Acquisition#injectionToDumpDelay"
        )
        self.delay_set = [
            "MKDTIM.ECA5.TSUA/Delay#delay",
            "MKDTIM.ECA5.TSUB/Delay#delay",
        ]
        self.energy_var = "MKD.ECA5.MKCB/PostOperation#energy"
        self.mkdh_volt_var12 = (
            "MKD.ECA5.MKCBI.MKDH12/PostOperation#signalCh1"
        )
        self.mkdh_volt_var3 = (
            "MKD.ECA5.MKCBI.MKDH3/PostOperation#signalCh1"
        )
        self.mkdv_volt_var12 = (
            "MKD.ECA5.MKCBI.MKDV12/PostOperation#signalCh1"
        )
        self.mkdv_volt_var3 = (
            "MKD.ECA5.MKCBI.MKDV3/PostOperation#signalCh1"
        )
        self.vars = [
            self.delay_var_name,
            self.energy_var,
            self.mkdh_volt_var12,
            self.mkdh_volt_var3,
            self.mkdv_volt_var12,
            self.mkdv_volt_var3,
        ]
        self.delay_start = japc.getParam(
            "MKDTIM.ECA5.TSUA/DelayLimits#delayMin"
        )
        self.delay_end = japc.getParam(
            "MKDTIM.ECA5.TSUA/DelayLimits#delayMax"
        )

        self.dump_delay = []
        self.dump_energy = []
        self.dump_h_volt12 = []
        self.dump_h_volt3 = []
        self.dump_v_volt12 = []
        self.dump_v_volt3 = []
        self.plot = False
        self.delay_to_set = 0.0

    def setParameter(self, value_to_set, **kwargs):

        if self.are_limits_ok(value_to_set):
            for delay_para in self.delay_set:
                self.japc.setParam(
                    delay_para,
                    value_to_set,
                    timingSelectorOverride=self.selector,
                    **kwargs
                )
            self.delay_to_set = value_to_set
        else:
            logging.warning("Set not done, out of limits!")

    def check_set(self, delay_machine):
        return (
            delay_machine - 1 <= self.delay_to_set <= delay_machine + 1
        )

    def are_limits_ok(self, set_value):
        return self.delay_start <= set_value <= self.delay_end

    def call_back_all_info(self, paramName, newValue):
        if all(ele is not None for ele in newValue):
            self.dump_delay.append(newValue[0])
            self.dump_energy.append(newValue[1])
            self.dump_h_volt12.append(newValue[2])
            self.dump_h_volt3.append(newValue[3])
            self.dump_v_volt12.append(newValue[4])
            self.dump_v_volt3.append(newValue[5])

    def getParameter(self, ele_name=None, getHeader=True, **kwargs):
        info = {}
        ele_name = self.vars if ele_name is None else ele_name
        kwargs["timingSelectorOverride"] = kwargs.get("timingSelectorOverride", self.selector)
        data = (
            self.japc.getParam(
                ele_name,
                getHeader,
                **kwargs
            )
            if getHeader
            else (
                self.japc.getParam(
                    ele_name,
                    getHeader,
                    **kwargs
                ),
                info,
            )
        )
        return data
