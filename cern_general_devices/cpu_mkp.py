import numpy as np
from cern_general_devices import Device
import logging


class CPUMKPAnalysis(Device):
    def __init__(self, japc, timingSelectorOverride=None):

        super().__init__(japc, "cpu mkp", timingSelectorOverride)

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.data = []

        self.cpu_diff_7a = "MKP.BA1.CPUTEMP.7A/Settings_PPM#delayCpuDiff"
        self.cpu_diff_8a = "MKP.BA1.CPUTEMP.8A/Settings_PPM#delayCpuDiff"
        self.cpu_vars = [self.cpu_diff_7a, self.cpu_diff_8a]

    def getParameter(self, getHeader=True, **kwargs):
        info = {}
        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        data = (
            self.japc.getParam(self.cpu_vars, getHeader, **kwargs)
            if getHeader
            else (
                self.japc.getParam(self.cpu_vars, getHeader, **kwargs),
                info,
            )
        )
        return data

    def get_delays(self):
        data, info = self.getParameter()
        data_ns = np.array(data) * 1e3
        return data_ns[:, 0], info


class CPUMKP(Device):
    def __init__(self, japc, timingSelectorOverride=None):

        super().__init__(japc, "cpu mkp", timingSelectorOverride)

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.data = []

        self.sp_var = "MKP.BA1.IPOC.CPU-UP.7A/Waveform#samplePeriod"
        self.cpu_up_7a = "MKP.BA1.IPOC.CPU-UP.7A/Waveform#waveformData"
        self.cpu_dw_7a = "MKP.BA1.IPOC.CPU-DW.7A/Waveform#waveformData"
        self.cpu_up_8a = "MKP.BA1.IPOC.CPU-UP.8A/Waveform#waveformData"
        self.cpu_dw_8a = "MKP.BA1.IPOC.CPU-DW.8A/Waveform#waveformData"
        self.cpu_vars = [
            self.cpu_up_7a,
            self.cpu_dw_7a,
            self.cpu_up_8a,
            self.cpu_dw_8a,
        ]
        self.sample_period = self.japc.getParam(self.sp_var) * 1e-6

    def getParameter(self, getHeader=True, **kwargs):
        info = {}
        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        data = (
            self.japc.getParam(self.cpu_vars, getHeader, **kwargs)
            if getHeader
            else (
                self.japc.getParam(self.cpu_vars, getHeader, **kwargs),
                info,
            )
        )
        return data

    def get_delays(self):
        delays = np.zeros(2)
        data, info = self.getParameter()
        delays[0] = (
            np.argmax(data[1]) - np.argmax(data[0])
        ) * self.sample_period
        delays[1] = (
            np.argmax(data[3]) - np.argmax(data[2])
        ) * self.sample_period
        return delays, info
