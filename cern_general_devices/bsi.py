import numpy as np
from cern_general_devices import Device
import logging
import pandas as pd


class BSI(Device):
    def __init__(self, japc, timingSelectorOverride=None):

        super().__init__(japc, "bsi", timingSelectorOverride)

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.data = []

        self.bsis_var = ["SEM_CONCENTRATOR_TT20/BeamIntensity"]


    def getParameter(self, ele_name=None, getHeader=True, **kwargs):
        info = {}
        ele_name = self.bsis_var if ele_name is None else ele_name
        kwargs["timingSelectorOverride"] = kwargs.get("timingSelectorOverride", self.selector)
        data_raw = self.japc.getParam(
                ele_name,
                getHeader,
                **kwargs
            )
        data_final = (self.clean_data(data_raw[0]), data_raw[1]) if getHeader else (self.clean_data(data_raw), info)
        return data_final

    def clean_data(self, data):
        # data[0].pop("intensity_adc_raw", None)
        data_new = {key: value for key, value in data[0].items() if key != "intensity_adc_raw"}
        return pd.DataFrame(data_new, data_new["device_name"])

    def get_intensity_time(self, getHeader=True, **kwargs):
        info = {}
        kwargs["timingSelectorOverride"] = kwargs.get("timingSelectorOverride", self.selector)
        data_raw = self.japc.getParam(
                self.bsis_var,
                getHeader,
                **kwargs
            )
        data_final = (self.clean_data_in_time(data_raw[0]), data_raw[1]) if getHeader else (self.clean_data_in_time(data_raw), info)

        return data_final
    def clean_data_in_time(self, data): 

        return pd.DataFrame(data[0]["intensity_adc_raw"].T, data[0]["device_name"]).apply(np.abs)
