import numpy as np
from cern_general_devices import Device
import logging
import pyjapc
import typing as t

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)


class TCDI(Device):
    def __init__(
        self,
        japc: pyjapc.PyJapc | None,
        tcdi_name: str,
        timingSelectorOverride: str | None = None,
        limits: np.ndarray = np.array([[-5, 15], [-15, 5]]),
    ) -> None:
        super().__init__(japc, "TCDI", timingSelectorOverride)

        if japc is None:
            japc = pyjapc.PyJapc("LHC.USER.ALL", noSet=True)
            japc.rbacLogin()
        self.japc = japc

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.is_tcdi_ready = False

        self.tcdi_name = tcdi_name.upper()
        self.plane = "x" if self.tcdi_name.split(".")[0][-1] == "H" else "y"

        self.limits = limits

        self.tcdi_settings_property = (
            f"{self.tcdi_name}/RequiredAbsolutePosition"
        )
        self.tcdi_measurements_property = (
            f"{self.tcdi_name}/MeasuredCornerPositions"
        )

        self.individual_lvdt_field = dict(
            left_do="lvdt_left_downstream",
            left_up="lvdt_left_upstream",
            right_do="lvdt_right_downstream",
            right_up="lvdt_right_upstream",
        )

        self.individual_positions_field = dict(
            left_do="left_downstream",
            left_up="left_upstream",
            right_up="right_upstream",
            right_do="right_downstream",
        )

        self.settings_property_template: t.Dict[
            str, t.Any
        ] = self.japc.getParam(
            self.tcdi_settings_property, timingSelectorOverride=""
        )

        self.beta = self._get_beta_lsa()
        gamma = np.sqrt(450**2 + 0.938**2) / 0.938
        self.beam_size = np.sqrt(3.5e-6 / gamma * self.beta) * 1e3

        self.centre, _ = self.get_settings_centre()
        self.half_gap = self.get_settings_half_gap()

        self.centre_sigma = self.centre / self.beam_size
        self.half_gap_sigma = self.half_gap / self.beam_size

        # assert self.are_positions_as_requested()

    def _get_beta_lsa(self) -> float:
        beta = self.japc.getParam(  # type: ignore
            f"rmi://virtual_lhc/{self.tcdi_name}/BBOptics#beta_{self.plane}",
            timingSelectorOverride="LHC.USER.INJ_PROT",
        )
        return beta

    def get_centre_lsa(self) -> float:
        centre = self.japc.getParam(  # type: ignore
            f"rmi://virtual_lhc/{self.tcdi_name}/BBCentre",
            timingSelectorOverride="LHC.USER.INJ_PROT",
        )
        return centre

    def check_jaw_angles(self, settings_property: t.Dict[str, t.Any]) -> None:
        if (
            settings_property["left_downstream"]
            != settings_property["left_upstream"]
        ):
            raise ValueError("Left jaw has an angle")
        if (
            settings_property["right_downstream"]
            != settings_property["right_upstream"]
        ):
            raise ValueError("Right jaw has an angle")

    def check_half_gap(
        self, settings_left: float, settings_right: float
    ) -> None:
        half_gap = np.abs(settings_left - settings_right) / 2
        LOG.info(f"Checking half gap: {half_gap}")
        current_half_gap = self.get_settings_half_gap()
        LOG.info(f"Current half gap: {current_half_gap}")
        if not np.isclose(half_gap, current_half_gap, atol=30e-3):
            raise ValueError(
                "Gap changed! Are you sure this is what you want?"
            )

    def get_settings_property(self) -> t.Dict[str, t.Any]:
        settings_property: t.Dict[str, t.Any] = self.japc.getParam(  # type: ignore
            self.tcdi_settings_property, timingSelectorOverride=""
        )
        return settings_property

    def get_settings_jaw_positions(self) -> t.Tuple[float, float]:
        settings_property = self.get_settings_property()
        left_requested_position = np.mean(
            [
                settings_property["left_downstream"],
                settings_property["left_upstream"],
            ]
        )
        right_requested_position = np.mean(
            [
                settings_property["right_downstream"],
                settings_property["right_upstream"],
            ]
        )
        self.check_jaw_angles(settings_property)
        return left_requested_position, right_requested_position

    def get_measured_jaw_positions(self) -> t.Tuple[float, float]:
        measured_positions_property = self.japc.getParam(  # type: ignore
            self.tcdi_measurements_property, timingSelectorOverride=""
        )
        measured_positions = {
            lvdt: measured_positions_property[lvdt_field]
            for lvdt, lvdt_field in self.individual_lvdt_field.items()
        }

        measured_left = np.mean(
            [
                measured_positions[jaw]
                for jaw in filter(
                    lambda x: "left" in x, measured_positions.keys()
                )
            ]
        )
        measured_right = np.mean(
            [
                measured_positions[jaw]
                for jaw in filter(
                    lambda x: "right" in x, measured_positions.keys()
                )
            ]
        )
        return measured_left, measured_right

    def get_measured_half_gap(self) -> float:
        measured_left, measured_right = self.get_measured_jaw_positions()
        return np.abs(measured_left - measured_right) / 2

    def get_measured_half_gap_sigma(self) -> float:
        return self.get_measured_half_gap() / self.beam_size

    def get_settings_half_gap(self) -> float:
        settings_left, settings_right = self.get_settings_jaw_positions()
        return np.abs(settings_left - settings_right) / 2

    def get_settings_half_gap_sigma(self) -> float:
        return self.get_settings_half_gap() / self.beam_size

    def get_measured_centre(self) -> t.Tuple[float, np.ndarray]:
        measured_jaws = self.get_measured_jaw_positions()
        return np.mean(measured_jaws), np.array(measured_jaws)

    def get_settings_centre(self) -> t.Tuple[float, np.ndarray]:
        settings_jaws = self.get_settings_jaw_positions()
        return np.mean(settings_jaws), np.array(settings_jaws)

    def get_measured_centre_sigma(self) -> float:
        centre, _ = self.get_measured_centre()
        return centre / self.beam_size

    def get_settings_centre_sigma(self) -> float:
        centre, _ = self.get_measured_centre()
        return centre / self.beam_size

    def set_half_gap_sigma(
        self, half_gap: float, no_set: bool = False
    ) -> np.ndarray:
        centre_now, _ = self.get_settings_centre()
        new_positions = centre_now + np.array(
            [half_gap * self.beam_size, -half_gap * self.beam_size]
        )
        LOG.info(f"Setting gap to {half_gap} from centre: {centre_now}")
        LOG.info(f"Setting jaws to {new_positions}")
        self.set_jaws(*new_positions, no_set=no_set)  # type: ignore
        return new_positions

    def set_centre(self, centre: float, no_set: bool = False) -> np.ndarray:
        centre_now, jaw_positions = self.get_settings_centre()
        delta_position = centre - centre_now
        new_positions = delta_position + jaw_positions
        LOG.info(f"initial positions: {jaw_positions}")
        LOG.info(f"Delta position: {delta_position}")
        LOG.info(f"Setting centre to {centre} from {centre_now}")
        LOG.info(f"Setting jaws to {new_positions}")
        self.check_half_gap(*new_positions)
        self.set_jaws(*new_positions, no_set=no_set)  # type: ignore
        return new_positions

    def set_centre_sigma(
        self, centre: float, no_set: bool = False
    ) -> np.ndarray:
        return self.set_centre(centre * self.beam_size, no_set=no_set)

    def are_positions_as_requested(self, atol: float = 50e-3) -> bool:
        measured_positions = self.get_measured_jaw_positions()
        requested_positions = self.get_settings_jaw_positions()
        LOG.info(f"Measured positions: {measured_positions}")
        LOG.info(f"Requested positions: {requested_positions}")
        LOG.info(
            f"error: {np.abs(np.array(measured_positions) - np.array(requested_positions))}"
        )

        return all(
            np.isclose(measured_positions, requested_positions, atol=atol)
        )

    def set_jaws(
        self,
        left_position: float | None = None,
        right_position: float | None = None,
        no_set: bool = False,
    ) -> None:
        print("Setting jaws")
        if no_set:
            LOG.warning("No set flag is set. Not setting jaws.")
        else:
            LOG.info(f"Setting jaws to {left_position}, {right_position}")
            self._arm_jaws(
                left_position=left_position, right_position=right_position
            )
            self._move_jaws()

    def is_in_range(self, positions: np.ndarray, ranges: np.ndarray) -> bool:
        return np.all((positions > ranges[:, 0]) & (positions < ranges[:, 1]))

    def test_movement_range(self, n_sigmas: float) -> bool:
        positions_max = self.set_centre_sigma(n_sigmas, no_set=True)
        LOG.info(f"Testing movement to {n_sigmas} sigmas: {positions_max}")
        # positions_min = self.set_centre_sigma(-n_sigmas, no_set=True)
        # LOG.info(f"Testing movement to {-n_sigmas} sigmas: {positions_min}")

        is_max_ok = self.is_in_range(positions_max, self.limits)
        # is_min_ok = self.is_in_range(positions_min, self.limits)

        # return is_max_ok and is_min_ok
        return is_max_ok

    def _arm_jaws(
        self,
        left_position: float | None = None,
        right_position: float | None = None,
    ) -> None:
        (
            present_left_position,
            present_right_position,
        ) = self.get_settings_jaw_positions()

        settings_property = dict()
        for key in self.settings_property_template.keys():
            if "left" in key:
                settings_property[key] = (
                    left_position
                    if left_position is not None
                    else present_left_position
                )
            elif "right" in key:
                settings_property[key] = (
                    right_position
                    if right_position is not None
                    else present_right_position
                )

        LOG.info(f"Arming jaws to {settings_property}")
        try:
            self.japc.setParam(  # type: ignore
                self.tcdi_settings_property,
                settings_property,
                timingSelectorOverride="",
                checkDims=False,
            )
        except Exception as e:
            LOG.warning(e)
            LOG.warning("Error in arming jaw movement")

    def _move_jaws(self) -> None:
        self.japc.setParam(  # type: ignore
            self.tcdi_name + "/SoftwareTrigger",
            {"sendTriggerMDC": True, "sendTriggerPRS": False},
            timingSelectorOverride="",
            checkDims=False,
        )
        LOG.info("Movement of collimator triggered.")

    def is_tcdi_waiting_command(self) -> bool:
        mdc_state = self.japc.getParam(  # type: ignore
            self.tcdi_name + "/MeasuredCornerPositions#mdcState",
            timingSelectorOverride="",
        )
        if mdc_state == 1:
            return True
        else:
            return False

    def getParameter(
        self,
        ele: str | None = None,
        getHeader: bool = True,
        **kwargs: t.Dict,
    ) -> t.Tuple[t.Any, t.Dict[t.Any, t.Any]]:
        measured_positions_property = self.japc.getParam(  # type: ignore
            self.tcdi_measurements_property,
            timingSelectorOverride="",
            getHeader=getHeader,
        )
        return measured_positions_property


if __name__ == "__main__":
    japc = pyjapc.PyJapc(noSet=True)
    japc.rbacLogin()

    tcdi = TCDI(japc=japc, tcdi_name="TCDIV.29011")

    print(tcdi.get_measured_centre_sigma())
    print(tcdi.get_settings_centre_sigma())
    print(tcdi.get_measured_centre())
    print(tcdi.get_settings_centre())
    print(tcdi.half_gap_sigma)
    print(tcdi.beam_size)

    tcdi.set_half_gap_sigma(10)

    tcdi.are_positions_as_requested()

    tcdi.test_movement_range(5)
