import numpy as np
from cern_general_devices import Device
import logging
import typing as t
import pyjapc
import copy


class TLlogical(Device):
    def __init__(
        self,
        japc: pyjapc.PyJapc,
        lsa_device: str,
        timingSelectorOverride: t.Optional[str] = None,
    ):
        super().__init__(japc, "TL_logical", timingSelectorOverride)

        if self.japc is not None:
            self.selector = (
                timingSelectorOverride
                if timingSelectorOverride is not None
                else self.japc.getSelector()
            )
        else:
            print("**** No JAPC instance provided")
            self.japc = pyjapc.PyJapc(noSet=True)

        self.lsa_device = lsa_device
        self.template, self.data_start = self._get_data_template()
        print(f"data_start = {self.data_start}")

    def _get_data_template(self) -> np.ndarray:
        data_temp = self.japc.getParam(
            self.lsa_device, timingSelectorOverride=self.selector
        )
        return np.array(data_temp), data_temp[1][2]

    def _transform_data_to_set(self, value_to_set: float) -> np.ndarray:
        data = copy.copy(self.template)
        data[1, 2] = value_to_set
        data[1, 3] = value_to_set
        if self.template[1, 4] is not np.NaN:
            data[1, 4] = value_to_set
        return data

    def set_k_element(self, value_to_set: float, logical: str) -> None:
        data_to_set = self._transform_data_to_set(value_to_set)
        self.japc.setParam(
            logical,
            data_to_set,
            timingSelectorOverride=self.selector,
        )

    def set_k(self, value_to_set: float) -> None:
        self.set_k_element(value_to_set, self.lsa_device)

    def get_k(self) -> t.Tuple[float, t.Dict[str, t.Any]]:
        data_raw, header = self.japc.getParam(
            self.lsa_device,
            timingSelectorOverride=self.selector,
            getHeader=True,
        )
        return data_raw[1][2], header

    def reset(self) -> None:
        self.set_k(self.data_start)

    def getParameter(
        self,
        ele: str | None = None,
        getHeader: bool = True,
        **kwargs: t.Dict[t.Any, t.Any],
    ) -> t.Tuple[t.Any, dict]:
        return self.get_k()

    def setParameter(self, **kwargs: t.Dict) -> None:
        assert "value_to_set" in kwargs
        self.set_k(kwargs["value_to_set"])  # type: ignore
