import numpy as np
import pyjapc
from cern_general_devices import Device
import logging


class BPMTFL(Device):
    def __init__(self, japc, timingSelectorOverride=None):

        super().__init__(japc, "bpm tt2", timingSelectorOverride)

        self.selector = (
            timingSelectorOverride
            if timingSelectorOverride is not None
            else self.japc.getSelector()
        )
        self.japc = japc
        self.data = []

        self.bpm_tt2_var = "BPMTFL_TT2/AcqSimple"

        self.bpm_tt2_acq = "BPMTFL_TT2/Acquisition"
        self.hor_var = "#horGatePos"
        self.ver_var = "#verGatePos"

        self.channel_names = self.japc.getParam(
            self.bpm_tt2_acq + "#bpmNames", timingSelectorOverride=self.selector
        )

        self.n_bpm = self.japc.getParam(self.bpm_tt2_acq + "#nbOfBpms", timingSelectorOverride=self.selector)

        self.all_h_vars = [
            self.bpm_tt2_var + self.hor_var
        ]
        self.all_v_vars = [
            self.bpm_tt2_var + self.ver_var
        ]

    def getParameter(self, ele_name=None, getHeader=True, **kwargs):
        info = {}
        ele_name = (
            [
                self.bpm_tt2_var + self.hor_var
            ]
            if ele_name is None
            else ele_name
        )
        kwargs["timingSelectorOverride"] = kwargs.get(
            "timingSelectorOverride", self.selector
        )
        data = (
            self.japc.getParam(ele_name, getHeader, **kwargs)
            if getHeader
            else (
                self.japc.getParam(ele_name, getHeader, **kwargs),
                info,
            )
        )
        return data

    def get_data_all(self):
        data_h_temp = self.japc.getParam(self.bpm_tt2_var + self.hor_var, timingSelectorOverride=self.selector)
        data_v_temp = self.japc.getParam(self.bpm_tt2_var + self.ver_var, timingSelectorOverride=self.selector)

        data_h, data_v = self.clean_data(
            data_h_temp, data_v_temp
        )
        return data_h, data_v

    def clean_data(self, data_h_temp, data_v_temp):
        data_h = data_h_temp[:self.n_bpm, :]
        data_v = data_v_temp[: self.n_bpm, :]
        return data_h, data_v

    def clean_first_inj(self, data_h, data_v):
        return data_h[:, 0], data_v[:, 0]

    def get_first_injection(self):
        data_h, data_v = self.get_data_all()
        return self.clean_first_inj(data_h, data_v)
